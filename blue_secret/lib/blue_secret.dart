import 'dart:async';

import 'package:flutter/services.dart';

class BlueSecret {
  static const MethodChannel _channel =
      const MethodChannel('blue_secret');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> get blueSecret async {
    final String secret = await _channel.invokeMethod('getBlueSecret');
    return secret;
  }


}
