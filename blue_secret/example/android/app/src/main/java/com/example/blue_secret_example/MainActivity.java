package com.example.blue_secret_example;

import io.flutter.embedding.android.FlutterActivity;

public class MainActivity extends FlutterActivity {
    //申明方法名
    private static final String BLUETOOTH_CHANNEL="setupBluetooth";

    private BluetoothManager bluetoothManager = null;	 //初始化
    private BluetoothAdapter bluetoothAdapter = null;	//蓝牙适配器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);


        new MethodChannel(getFlutterView(),BLUETOOTH_CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if(methodCall.method.equals("openBuleTooth")){	//判断flutter调用那个方法
                            if(supportBuleTooth()){						//检测真机是否支持蓝牙
                                openBuleTooth();							//打开蓝牙
                                result.success("蓝牙已经被开启");
                            }else{
                                result.error("设备不支持蓝牙",null,null);
                            }
                        }
                        else if(methodCall.method.equals("getBuleTooth")){
                            if(supportBuleTooth()){
                                if(disabled()){								//检测蓝牙的状态
                                    result.success("蓝牙已经开启");
                                }else{
                                    result.success("蓝牙未开启");
                                }
                            }
                        }
                    }
                }
        );
    }

    //是否支持蓝牙

    private boolean supportBuleTooth(){


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bluetoothManager=(BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bluetoothAdapter=bluetoothManager.getAdapter();
        }
        if (bluetoothAdapter==null){    //不支持蓝牙
            return false;
        }
        return true;
    }

    //打开蓝牙
    private void openBuleTooth(){
        //判断蓝牙是否开启
        Intent enabler=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enabler,1);
    }

    //判断蓝牙是否已经开启
    private boolean disabled(){
        if(bluetoothAdapter.isEnabled()){
            return true;
        }
        return false;
    }
}
