package com.example.blue_secret;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import com.cxx.a3300kt.blesecret;
import com.cxx.a3300kt.autochip;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.os.Handler;

/** BlueSecretPlugin */
public class BlueSecretPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  Handler mHandler = new Handler();
  boolean mScanning = true;
  BluetoothAdapter mBluetoothAdapter;
  final int SCAN_PERIOD = 10000;

  public final int CAR_INFO = 1;
  public final int CAR_OPENDOOR = 2;
  public final int CAR_ONOFF = 3;
  public final int CAR_AUTODISTINGUISH = 4;
  public final int CAR_MAINCHIP = 5;
  public final int CAR_SAFECHIP1 = 6;
  public final int CAR_SAFECHIP2 = 7;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "androidBluetooth");
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "androidBluetooth");
    channel.setMethodCallHandler(new BlueSecretPlugin());
  }

//  public void scanLeDevice(final boolean enable, int newscan, Context context) {
//    if (enable) {
//
//      Log.d("aabbcc", "scanledevice" + enable);
//      // Stops scanning after a pre-defined scan period.
//      //application.mBluetoothLeService.disconnect();
//
//      mHandler.postDelayed(new Runnable() {
//        public void run() {
//          mScanning = false;
//          mBluetoothAdapter.stopLeScan(mLeScanCallback);
//          Log.d("aabbcc", "mLeScanCallback stop" + enable);
//        }
//      }, SCAN_PERIOD);
//      mScanning = true;
//      //UUID[] uuid={UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb")};
//      //mBluetoothAdapter.startLeScan(uuid,mLeScanCallback);
//
//      mBluetoothAdapter.startLeScan(mLeScanCallback);
//      dev_exist = 0;
////				if (1==newscan){
////					registerBroadcast(context);
////					doDiscovery();
////				}
//
//    } else {
//      mScanning = false;
//      mBluetoothAdapter.stopLeScan(mLeScanCallback);
//    }
//    //invalidateOptionsMenu();
//  }
//
//  private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
//
//    public void onLeScan(final BluetoothDevice device, int rssi, final byte[] scanRecord) {
//      //Log.d("aabbcc", "device:" + device.getName());
//      //Log.d("aabbcc", "device:" + byte2HexStr(scanRecord));
//
//      synchronized (this) { //同步代码
//        BleScanRecord.g_check = -1;
//        //BleScanRecord.g_devid;
//        BleScanRecord test = BleScanRecord.parseFromBytes(scanRecord);
//        Log.d("aabbcc", "app device:" + byte2HexStr(scanRecord));
//        Log.d("aabbcc", "dev_name:" + BleScanRecord.g_devid + "app dev_name" + app.dev_name);
//        Log.d("aabbcc", "net key:" + app.bt_key);
//        Log.d("aabbcc", "g_check:" + test.g_check);
//        if (test.g_check != 0)
//          return;
//        //System.out.println("连接状态设备===" + byte2HexStr(scanRecord));
//        //Log.d("aabbcc", "dev_name:" + BleScanRecord.g_devid + "app dev_name" + app.dev_name);
//
//
//        //if(app.dev_name != null && app.dev_name.contains(BleScanRecord.g_devid)  )
////                String addr;
////                //app.bt_key = "A1A2A3A4A5A6A7A8B1B2B3B4B5B6B7B8";
////                addr = device.getAddress();
////                app.mDeviceAddress = addr.trim();
////                String deviceaddress = getSharedPreferences("login", MODE_PRIVATE).getString("deviceaddress","");
////                System.out.println("=="+app.mDeviceAddress+"==="+deviceaddress+"=="+app.dev_name+"=="+app.bt_key);
//
//        if ((app.dev_name != null && BleScanRecord.g_devid!=null&&(app.dev_name.contains(BleScanRecord.g_devid)))&&app.isjudge==0)//863081033200516 863081033200508
//        {
//
//          String addr;
//          //app.bt_key = "A1A2A3A4A5A6A7A8B1B2B3B4B5B6B7B8";
//          addr = device.getAddress();
//          app.mDeviceAddress = addr.trim();
//          if (mScanning) {
//            mBluetoothAdapter.stopLeScan(mLeScanCallback);
//            mScanning = false;
//          }
//          Log.d("aabbcc", "application.mDeviceAddress" + app.mDeviceAddress);
//          new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//
//              connect(app.mDeviceAddress);//连接蓝牙
//              dev_exist = 1;
//              secretflg = -1;
//            }
//          }, 800);
//          return;
//        }
//
//        String addr;
//        //app.bt_key = "A1A2A3A4A5A6A7A8B1B2B3B4B5B6B7B8";
//        addr = device.getAddress();
//        String scanAdress = addr.trim();
//        final String deviceaddress = getSharedPreferences("login", MODE_PRIVATE).getString("deviceaddress","");
//        System.out.println("=="+scanAdress+"==="+deviceaddress+"=="+app.dev_name+"=="+app.bt_key);
//        if((!TextUtils.isEmpty(scanAdress)&&!TextUtils.isEmpty(deviceaddress)&&scanAdress.equals(deviceaddress))){
//          app.mDeviceAddress=scanAdress;
//          if (mScanning) {
//            mBluetoothAdapter.stopLeScan(mLeScanCallback);
//            mScanning = false;
//          }
//          Log.d("aabbcc", "application.mDeviceAddress" + app.mDeviceAddress);
//          new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//
//              connect(app.mDeviceAddress);//连接蓝牙
//              dev_exist = 1;
//              secretflg = -1;
//            }
//          }, 800);
//
//        }
//
//      }
//
//
//    }
//
//  };

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    /*if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else {
      result.notImplemented();
    }*/

    if (call.method.equals("package_send")) {
      byte[] recdata = call.argument("package");
      byte[] out = autochip.sendpack(recdata, 128);
      result.success(out);
    }

    if (call.method.equals("packageEnd")) {
      byte[] recdata = call.argument("package");
      byte[] out = autochip.endpack(recdata, recdata.length);
      result.success(out);
    }

    if (call.method.equals("scan")){
      mHandler.postDelayed(new Runnable() {
        public void run() {
          mScanning = false;
//          mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
      }, SCAN_PERIOD);
      mScanning = true;

//      mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    switch (call.method){
      case "getPlatformVersion":
        result.success("Android " + android.os.Build.VERSION.RELEASE);
        break;
      case "getBlueSecret":

        String resultSecret=call.argument("resultSecret");
        byte[] recdata = hexStr2Bytes(resultSecret);

        String bt_key=call.argument("deviceSecret");
        int serial_cnt = call.argument("serial_cnt");

        //收到了flutter消息==============resultSecret:[B@8120072---81010001C51A4B2841E627D47D72C34079BE1F6C =========deviceSecret:6421374045557a406446424e23667721
        System.out.println("收到了flutter消息==============resultSecret:"+resultSecret+"===recdata:"+recdata+"=========deviceSecret:"+ bt_key);

        byte [] seed = new byte[recdata.length - 4];
        for(int i=0;i< seed.length;i++)
        {
          seed[i] = recdata[4 + i];
        }

        byte [] netkey = hexStr2Bytes(bt_key);

        byte [] newkey = blesecret.encryptinit(seed,netkey);
        byte [] outseed = new byte[40];

        outseed = blesecret.encrypt(seed,outseed,8);

        byte[] data = new byte[]{(byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0};
        data = new byte[4 + outseed.length];
        data[0] = 0x1;
        data[1] = 3;
        data[2] = (byte) (serial_cnt / 256);
        data[3] = (byte) (serial_cnt % 256);

        for(int j = 0;j < outseed.length;j++)
        {
          data[4 + j] = outseed[j];
        }

        result.success(byte2HexStr(data));

        //result.success("收到了flutter消息==============resultSecret:"+resultSecret+" =========deviceSecret:"+ deviceSecret);
        break;
      case "doMatch":
        int cmd = call.argument("cmd");
        serial_cnt = call.argument("serial_cnt");

        System.out.println("收到了flutter消息=====cmd:"+cmd+"====serial_cnt:"+ serial_cnt);

        data = new byte[9];//{(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 129, 0x0};
        data[0] = (byte) 0x0f;
        data[1] = (byte) 5;
        data[2] = (byte) (serial_cnt / 256);
        data[3] = (byte) (serial_cnt % 256);
        data[4] = (byte) 0x40;
        data[5] = (byte) 0x00;
        data[6] = (byte) 0x01;
        serial_cnt = serial_cnt + 1;
        if (serial_cnt > 0xffff) serial_cnt = 0;
        switch (cmd) {
          case CAR_INFO:
            data[7] = (byte) 0x00;
            data[8] = (byte) 0x08;
            break;
          case CAR_OPENDOOR:
            data[7] = (byte) 0x10;
            data[8] = (byte) 0x01;
            break;
          case CAR_ONOFF:
            data[7] = (byte) 0x10;
            data[8] = (byte) 0x02;
            break;
          case CAR_AUTODISTINGUISH:
            data[7] = (byte) 0x00;
            data[8] = (byte) 0xA0;
            break;
          case CAR_MAINCHIP:
            data[7] = (byte) 0x00;
            data[8] = (byte) 0x20;
            break;
          case CAR_SAFECHIP1:
            data[7] = (byte) 0x00;
            data[8] = (byte) 0x21;
            break;
          case CAR_SAFECHIP2:
            data[7] = (byte) 0x00;
            data[8] = (byte) 0x22;
            break;
        }
        result.success(byte2HexStr(data));
        break;


      case "onlineMatch":
        serial_cnt = call.argument("serial_cnt");
        byte[] dataa3;
        byte[] resultData3 = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x08};
//        data = new byte[20];//{(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 129, 0x0};
        dataa3 = new byte[4 + resultData3.length];
        dataa3[0] = 0x0f;
        dataa3[1] = (byte) resultData3.length;
        dataa3[2] = (byte) (serial_cnt / 256);
        dataa3[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < resultData3.length; i++) {
          dataa3[4 + i] = resultData3[i];
        }
        result.success(byte2HexStr(dataa3));
        break;

      case "bleSendData":
        serial_cnt = call.argument("serial_cnt");
        int offset = call.argument("offset");
        String tempStr = call.argument("hexString");

        byte[] dataa4 = hexStr2Bytes(tempStr);
        System.out.println("原始数据=====:" + dataa4 + "=====offset" + offset + "=====tempStr" + tempStr);
        int total = dataa4.length;
        int left = total - offset;
        int len = left > 16 ? 16 : left;
//        System.out.println("发送数据" + dataa4 + "total" + total);
        byte[] packet = new byte[20];
        packet[0] = 0x0f;
        packet[1] = (byte) left;
        packet[2] = (byte) (serial_cnt / 256);
        packet[3] = (byte) (serial_cnt % 256);

        for (int i = 0; i < len; i++) {
          packet[i + 4] = dataa4[offset + i];
        }
        //if (serial_cnt > 0xffff) serial_cnt = 0;
        System.out.println("发送的数据: " + byte2HexStr(packet) + "===total: " + total);
        result.success(byte2HexStr(packet));
        break;
      case "setCarType":
        serial_cnt = call.argument("serial_cnt");
        byte[] dataa11;
        byte[] resultData11 = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x0B};
//        data = new byte[20];//{(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 129, 0x0};
        dataa11 = new byte[4 + resultData11.length];
        dataa11[0] = 0x0f;
        dataa11[1] = (byte) resultData11.length;
        dataa11[2] = (byte) (serial_cnt / 256);
        dataa11[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < resultData11.length; i++) {
          dataa11[4 + i] = resultData11[i];
        }
        result.success(byte2HexStr(dataa11));
        break;
      case "autoDistinguish":
        serial_cnt = call.argument("serial_cnt");
        byte[] autoData;
        byte[] autoResultData = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0xA0};
//        data = new byte[20];//{(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 129, 0x0};
        autoData = new byte[4 + autoResultData.length];
        autoData[0] = 0x0f;
        autoData[1] = (byte) autoResultData.length;
        autoData[2] = (byte) (serial_cnt / 256);
        autoData[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < autoResultData.length; i++) {
          autoData[4 + i] = autoResultData[i];
        }
        result.success(byte2HexStr(autoData));
        break;
      case "audiAction1":
        serial_cnt = call.argument("serial_cnt");
        byte[] audiData1;
        byte[] audiResultData1 = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x10, (byte) 0x01};
//        data = new byte[20];//{(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 129, 0x0};
        audiData1 = new byte[4 + audiResultData1.length];
        audiData1[0] = 0x0f;
        audiData1[1] = (byte) audiResultData1.length;
        audiData1[2] = (byte) (serial_cnt / 256);
        audiData1[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < audiResultData1.length; i++) {
          audiData1[4 + i] = audiResultData1[i];
        }
        result.success(byte2HexStr(audiData1));
        break;
      case "audiAction2":
        serial_cnt = call.argument("serial_cnt");
        byte[] audiData2;
        byte[] audiResultData2 = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x10, (byte) 0x02};
//        data = new byte[20];//{(byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 129, 0x0};
        audiData2 = new byte[4 + audiResultData2.length];
        audiData2[0] = 0x0f;
        audiData2[1] = (byte) audiResultData2.length;
        audiData2[2] = (byte) (serial_cnt / 256);
        audiData2[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < audiResultData2.length; i++) {
          audiData2[4 + i] = audiResultData2[i];
        }
        result.success(byte2HexStr(audiData2));
        break;
        //
      case "carstyleData":
        serial_cnt = call.argument("serial_cnt");
        byte[] carstyleData;
        byte[] carstyleResultData = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x0B, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x0D};
        carstyleData = new byte[4 + carstyleResultData.length];
        carstyleData[0] = 0x0f;
        carstyleData[1] = (byte) carstyleResultData.length;
        carstyleData[2] = (byte) (serial_cnt / 256);
        carstyleData[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < carstyleResultData.length; i++) {
          carstyleData[4 + i] = carstyleResultData[i];
        }
        result.success(byte2HexStr(carstyleData));
        break;
      case "keyMatch":
        serial_cnt = call.argument("serial_cnt");
        int keyCount = call.argument("keyCount");
        System.out.println("钥匙数量: " + (byte)keyCount);
        byte[] keyMatchData;
        byte[] keyMatchResultData = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x10, (byte) 0x04, (byte) keyCount};
        keyMatchData = new byte[4 + keyMatchResultData.length];
        keyMatchData[0] = 0x0f;
        keyMatchData[1] = (byte) keyMatchResultData.length;
        keyMatchData[2] = (byte) (serial_cnt / 256);
        keyMatchData[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < keyMatchResultData.length; i++) {
          keyMatchData[4 + i] = keyMatchResultData[i];
        }
        result.success(byte2HexStr(keyMatchData));
        break;

      case "audiKeyMatch":
        serial_cnt = call.argument("serial_cnt");
        int audiKeyCount = call.argument("keyCount");
        System.out.println("钥匙数量: " + (byte)audiKeyCount);
        byte[] audiKeyMatchData;
        byte[] audiKeyMatchResultData = new byte[]{(byte) 0x40, (byte) 0x00, (byte) 0x01, (byte) 0x10, (byte) 0x06, (byte) audiKeyCount};
        audiKeyMatchData = new byte[4 + audiKeyMatchResultData.length];
        audiKeyMatchData[0] = 0x0f;
        audiKeyMatchData[1] = (byte) audiKeyMatchResultData.length;
        audiKeyMatchData[2] = (byte) (serial_cnt / 256);
        audiKeyMatchData[3] = (byte) (serial_cnt % 256);
        for (int i = 0; i < audiKeyMatchResultData.length; i++) {
          audiKeyMatchData[4 + i] = audiKeyMatchResultData[i];
        }
        result.success(byte2HexStr(audiKeyMatchData));
        break;
    }
  }

  /**
   * bytes转换成十六进制字符串
   * @param byte[] b byte数组
   * @return String 每个Byte值之间空格分隔
   */
  public static String byte2HexStr(byte[] b)
  {
    String stmp="";
    StringBuilder sb = new StringBuilder("");
    for (int n=0;n<b.length;n++)
    {
      stmp = Integer.toHexString(b[n] & 0xFF);
      sb.append((stmp.length()==1)? "0"+stmp : stmp);
      sb.append("");
      //sb.append(" ");
    }
    return sb.toString().toUpperCase().trim();
  }

  /**
   * bytes字符串转换为Byte值
   * @param String src Byte字符串，每个Byte之间没有分隔符
   * @return byte[]
   */

  public static byte[] hexStr2Bytes(String src)
  {
    int m=0,n=0;
    int l=src.length()/2;
    System.out.println(l);
    byte[] ret = new byte[l];
    for (int i = 0; i < l; i++)
    {
      m=i*2+1;
      n=m+1;
      ret[i] = (byte)(Integer.decode("0x" + src.substring(i*2, m) + src.substring(m,n)) & 0xff);
      //ret[i] = Byte.decode("0x" + src.substring(i*2, m) + src.substring(m,n));
    }
    System.out.println("字符串===" + src + "=====结果: " + ret);
    return ret;
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }
}
