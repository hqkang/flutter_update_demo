//package android.src.main.java.com.example.flutter_plugin_update;
package com.example.flutter_plugin_update;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.os.Handler;
import android.os.Looper;

import io.flutter.plugin.common.PluginRegistry.Registrar;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;
import java.util.LinkedList;

import com.google.gson.Gson;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * FlutterPluginUpdatePlugin
 */
public class FlutterPluginUpdatePlugin implements FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private MethodChannel channel;

    static {
        System.loadLibrary("socket_server");
    }

    public static native void startServer(int port);

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_plugin_update");
        channel.setMethodCallHandler(new FlutterPluginUpdatePlugin());
    }

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flutter_plugin_update");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, final @NonNull Result result) {
        switch (call.method) {
            case "getPlatformVersion":
                result.success("Android " + android.os.Build.VERSION.RELEASE);
                break;
//            case "installBegin" :
//                getInstance().handleBluetoothData("{\"Type\":2,\"NumCnt\":0,\"Data\":\"123\",\"CurrentSize\":1024,\"DataSize\":3,\"Status\":0}");
//                break;
            case "startUpdate":
                String data = call.argument("data");
                getInstance().startUpdate(data,
                        new SocketCallback() {
                            @Override
                            public void sendToDevice(String data) {
                                System.out.println("1sendToDevice ———— " + data);
//                                result.success(data);

                                final String v = String.valueOf(data);
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        System.out.println("2sendToDevice ———— " + v);
                                        result.success(v);
                                    }
                                });
                            }
                        });
                break;
            case "handleBluetoothData":
                String bluetoothData = call.argument("bluetoothData");
                JsonData json = new JsonData();
                json.PackNum = 0;
                json.Type = mType;
                json.SubType = mSubType;
                json.Status = 0;
                json.Data = bluetoothData;
                Gson gson = new Gson();
                String jsonValue = gson.toJson(json);
                getInstance().handleBluetoothData(jsonValue);
                break;
            case "retriveData":
                String v = "";
                if (dataList.size() >0) {
                    v = String.valueOf(dataList.poll());
                }

                System.out.println("1retriveData ———— " + v);

                result.success(v);
                break;
            case "initSocketUtil":
                String ip = call.argument("ip");
                System.out.println("ip: " + ip);
                getInstance().init(ip);
                break;
            default:
                result.notImplemented();
                break;
        }

//        if (call.method.equals()) {
//            result.success("Android " + android.os.Build.VERSION.RELEASE);
//        } else {
//            result.notImplemented();
//        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    public static Handler mHandler = new Handler(Looper.getMainLooper());

    public static UpdateSocketUtil getInstance() {
        return UtilHolder.instance;
    }

    private static class UtilHolder {
        private static UpdateSocketUtil instance = new UpdateSocketUtil();
    }

    private static String mIp = "";
    private static int mType;
    private static int mSubType;
    private static LinkedList<String> dataList = new LinkedList<>();

    public static class UpdateSocketUtil {

        static {
            System.loadLibrary("socket_server");
        }

        private int port = 0;

        private static final String TAG = "UpdateSocket";

        private SocketCallback mCallback;

        private Socket socket;

        private UpdateSocketUtil.WriteThread writeThread;
        private UpdateSocketUtil.ReadThread readThread;

        private UpdateSocketUtil() {
        }

        public void init(final String ip) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Random r = new Random();
                    port = r.nextInt(9999);
                    mIp = ip;
                    System.out.println("port = " + port);
                    startServer(port);
                }
            }).start();
        }

        // 开始升级
        public void startUpdate(final String data, SocketCallback callback) {
            this.mCallback = callback;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(TAG + "连接c服务器..");
                        //取连接客户端
                        socket = new Socket();

                        socket.connect(new InetSocketAddress(mIp, port));

                        writeThread = new UpdateSocketUtil.WriteThread(socket, data, 1);
                        writeThread.start();

                        if (null == readThread) {
                            readThread = new UpdateSocketUtil.ReadThread(socket);
                            readThread.start();
                        }

                        if (socket.isConnected()) {

                            while (true) {
                                // 读数据

                                InputStream ips = socket.getInputStream();

                                InputStreamReader ipsr = new InputStreamReader(ips, "UTF-8");

                                char[] chars = new char[512];
                                ipsr.read(chars, 0, chars.length);
//                                Log.e(TAG, "接收的全部 Json  —— " + String.valueOf(chars));


                                for (int i = 0; i < chars.length; i++) {
                                    if (String.valueOf(chars[i]).equals("}")) {
                                        int valuePlace = i + 1;
                                        Log.e(TAG, "接收的 place —— " + valuePlace);
                                        if (valuePlace < 5) break;
                                        char[] realValue = new char[valuePlace];
                                        System.arraycopy(chars, 0, realValue, 0, valuePlace);
                                        Log.e(TAG, "接收的 realValue —— \n" + String.valueOf(realValue));

                                        try {
                                            Gson gson = new Gson();
                                            JsonData data = gson.fromJson(String.valueOf(realValue), JsonData.class);
                                            mType = data.Type;
                                            mSubType = data.SubType;

                                            switch (data.Type) {
                                                case 1:
                                                    int status = data.Status;
                                                    switch (data.SubType) {
                                                        case 1:
                                                            if (status == 1) {
                                                                dataList.clear();
                                                                JsonData json = new JsonData();
                                                                json.PackNum = 0;
                                                                json.Type = 1;
                                                                json.SubType = 2;
                                                                json.Status = 0;
                                                                json.Data = "";
                                                                json.DataSize = 0;
                                                                String jsonValue = gson.toJson(json);

                                                                handleBluetoothData(jsonValue);
                                                            }
                                                            break;
                                                        case 2:
//                                                            mCallback.sendToDevice(String.valueOf(data.Data));
                                                            dataList.add(String.valueOf(data.Data));
                                                            break;
                                                        case 3:

                                                            break;
                                                    }
                                                    break;
                                            }
                                            Log.e(TAG, "JsonData —— data:" + String.valueOf(data.Data));


                                        } catch (Exception e) {
                                            Log.e(TAG, "error —— " + e);
                                        }
                                        break;
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        // 处理来自蓝牙设备的数据
        public void handleBluetoothData(final String jsonData) {
            try {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            System.out.println(TAG + "向c服务器写数据===" + jsonData);
                            writeThread.out.write(jsonData.getBytes("UTF-8"));
                            writeThread.out.flush();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        static class WriteThread extends Thread {
            DataOutputStream out = null;
            String mData = "";
            int mType;

            public WriteThread(Socket socket, String data, int type) {
                try {
                    out = new DataOutputStream(socket.getOutputStream());
                    mData = data;
                    mType = type;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                super.run();
                try {

                    Log.e(TAG, "向c服务器写数据===");


                    JsonData jsonData = new JsonData();
                    jsonData.DataSize = mData.length() / 2;
//                    jsonData.DataSize = 0;
                    jsonData.PackNum = 0;
                    jsonData.Type = 1;
                    jsonData.SubType = 1;
                    jsonData.Status = 0;
                    jsonData.Data = mData;
//                    jsonData.Data = "";
                    Gson gson = new Gson();
                    String jsonValue = gson.toJson(jsonData);

                    Log.e(TAG, "length = " + jsonValue.length());
                    Log.e(TAG, "dataSize = " + mData.length());
                    Log.e(TAG, "type = " + jsonData.Type);
                    Log.e(TAG, "subType = " + jsonData.SubType);
                    Log.e(TAG, "status = " + jsonData.Status);
                    System.out.println("最后一个字节为 } ? ———— " + jsonValue.endsWith("}"));

                    byte[] v = jsonValue.getBytes("UTF-8");
                    out.write(v);
                    out.flush();
//                    Log.e(TAG, jsonValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        static class ReadThread extends Thread {
            BufferedReader bufferedReader;
            DataOutputStream out;

            public ReadThread(Socket socket) {
                try {
                    bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    out = new DataOutputStream(socket.getOutputStream());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                super.run();
                //循环读取内容
                while (true) {
                    try {
                        sleep(3000);
//                    byte d = 0x1A;
                        byte[] v = "A".getBytes("UTF-8");
                        out.write(v);
                        out.flush();
                        Log.e("beat", "发送心跳：" + "A");
                    } catch (Exception e) {
                        Log.e(TAG, "ReadThread error ———— " + e.toString());
                    }
                }
            }
        }

    }

    public static String getIpAddress(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if (info != null && info.isConnected()) {
            // 3/4g网络
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                try {
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }

            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                //  wifi网络
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ipAddress = intIP2StringIP(wifiInfo.getIpAddress());
                return ipAddress;
            } else if (info.getType() == ConnectivityManager.TYPE_ETHERNET) {
                // 有限网络
                return getLocalIp();
            }
        }
        return null;
    }

    private static String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }


    // 获取有限网IP
    private static String getLocalIp() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()
                            && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {

        }
        return "0.0.0.0";
    }

    static class JsonData {
        public int Type;                    // 功能码
        public int SubType;                 // 副功能码
        public int PackNum;                  // Package编号
        public String Data;                 // Package数据
        public int DataSize;                // 数据总大小
        public int Status;                  // 状态 0 —— 正常，1 —— 异常
    }

    public interface SocketCallback {
        void sendToDevice(String data);

//        void updateMessage();
    }
}
