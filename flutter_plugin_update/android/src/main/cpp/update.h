
#ifndef __UPDATE__H
#define __UPDATE__H

#ifdef __cplusplus
extern "C"
{
#endif

#pragma pack(1)
//定义蓝牙文件更新数据传输的帧结构体
struct BlePackageData {
    uint16_t Head;      // 0x05表示更新模块 0x01表示查询SFD
    uint16_t PackNum;   //表示消息的序号
    uint8_t Data[256];  //表示CAN传输数据
};
#pragma pack()

typedef enum {
    EM_DOWNLOAD_CONTINUE = 0x00000000,          //通知未发完，继续下一包传输
    EM_DOWNLOAD_END = 0x00000001,           //下载完成
    EM_UPDATE_SUCCEED = 0x00000002,         //更新数据成功
} EM_05_ACK_SUCCEED;

typedef enum {
    EM_PROTOCOL_ERROR = 0x00000001,         //协议出错
    EM_PACKNUM_ERROR = 0x00000002,          //包序列号出错
    EM_BYTE_ERROR = 0x00000003,             //有效字节出错
    EM_LASTDATA_SENDING = 0x00000004,       //上一条消息仍处于发送状态
    EM_CAN_BUSY = 0x00000005,               //CAN总线繁忙
    EM_BCM_ACK_TIMEOUT = 0x00000101,        //BCM响应超时
    EM_BCM_TIMEOUT = 0x00000102,            //BCM响应超时
    EM_TIMEOUT = 0x00000104,                //超时
    EM_MESSAGE_ERROR = 0x00000106,          //消息错误
    EM_DREDGE_SFD_NO_REPLY = 0x00000107,    //开通SFD命令无回复
    EM_SEARCH_SFD_ERROR = 0x00000108,       //查询SFD操作出错
    EM_CHECK_SFD_STATE = 0x00000300,        //检查SFD状态
} EM_05_ACK_ERROR;

extern void *pthread_update_func(void *arg);

extern void *pthread_reg_func(void *arg);

#ifdef __cplusplus
}
#endif

#endif
