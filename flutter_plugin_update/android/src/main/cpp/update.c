#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include "cJSON.h"
#include "package.h"
#include "update.h"

extern int clientfd;
extern struct SocketPackageData thread_datapack[5];
struct BlePackageData blepackdata;//蓝牙协议包
struct SocketPackageData jsonpackdata;//json协议包
uint8_t update_file[1024 * 512];
uint32_t filelen;//文件总大小
uint32_t send_len = 0;//已发送的文件大小
uint32_t ack_state;
int timeout; //超时时间
int step;//根据应答决定下一步

static void
pack_ble_to_json(struct SocketPackageData *json, struct BlePackageData *ble,
                 int nLen) {
    HexToStr(&json->Data[0], (uint8_t *) &ble->Head, 1);
    HexToStr(&json->Data[2], (uint8_t *) &ble->PackNum, 1);
    HexToStr(&json->Data[4], ble->Data, nLen);
}

static int wait_ack_from_mf(void)
{
    if (thread_datapack[1].Data[0] == 0x05) {
        ack_state = get_return_state(&thread_datapack[1].Data[2]);
        //更新超时时间
        timeout = time((time_t *) NULL);
        if (thread_datapack[1].Data[1] == 0x80) {
            switch (ack_state) {
                case EM_DOWNLOAD_CONTINUE:
                    step = 1;
                    break;
                case EM_DOWNLOAD_END:
                    step = 2;
                    break;
                case EM_UPDATE_SUCCEED:
                    step =3;
                    break;
                default:
                    LOGE("无对应成功响应数据");
                    break;
            }
        } else if (thread_datapack[1].Data[1] == 0x8F) {
            switch (ack_state) {
                case EM_PROTOCOL_ERROR:
                    break;
                case EM_PACKNUM_ERROR:
                    break;
                case EM_BYTE_ERROR:
                    break;
                case EM_LASTDATA_SENDING:
                    break;
                case EM_CAN_BUSY:
                    break;
                case EM_BCM_ACK_TIMEOUT:
                    break;
                case EM_BCM_TIMEOUT:
                    break;
                case EM_TIMEOUT:
                    break;
                case EM_MESSAGE_ERROR:
                    break;
                case EM_DREDGE_SFD_NO_REPLY:
                    break;
                case EM_SEARCH_SFD_ERROR:
                    break;
                case EM_CHECK_SFD_STATE:
                    break;
                default:
                    LOGE("无对应失败响应数据");
                    break;
            }
            return 1;
        }
        memset(thread_datapack[1].Data, 0, sizeof(sizeof(thread_datapack[1].Data) /
                                            sizeof(thread_datapack[1].Data[0])));
    }else
    {
        if (time((time_t *) NULL) - timeout > 5) {
            LOGE("升级文件超时");
            return 1;
        }
    }
    return 0;
}

static void send_ack_to_app(EM_UPDATE_STATUS state) {
    char *csend;
    jsonpackdata.Type = thread_datapack[1].Type;
    jsonpackdata.SubType = thread_datapack[1].SubType;
    jsonpackdata.Status = state;
    csend = send_json(jsonpackdata);
    LOGE("发送数据=%s\n", csend);
    send(clientfd, csend, strlen(csend), 0);
    free(csend);
}

static int download_to_mf(void) {
    //判断是否有文件
    if (filelen != 0) {
        if(send_len == 0)
        {
            //更新超时时间
            timeout = time((time_t *) NULL);
        }
        //判断文件是否已经发完
        if (filelen != send_len) {
            //第一包数据
            if (send_len == 0) {
                blepackdata.Head = 0x05;
                blepackdata.PackNum = 0;
                memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
                //判断是否只有一包
                if ((filelen - send_len) < 128) {
                    memcpy(blepackdata.Data, update_file,
                           (filelen - send_len) * sizeof(uint8_t));
                    pack_ble_to_json(&jsonpackdata, &blepackdata,
                                     (int) (filelen - send_len));
                    jsonpackdata.DataSize = (int) (filelen - send_len + 2);
                    send_len += (filelen - send_len);
                } else {
                    memcpy(blepackdata.Data, &update_file[send_len], 128 * sizeof(uint8_t));
                    pack_ble_to_json(&jsonpackdata, &blepackdata, 128);
                    jsonpackdata.DataSize = 128 + 2;
                    send_len += 128;
                }
                send_ack_to_app(EM_STATUS_DEF);
            } else {
                //收到继续发送下一包的指令
                if (step == 1) {
                    step = 0;
                    memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
                    blepackdata.PackNum++;
                    //判断是否是最后一包数据
                    if ((filelen - send_len) < 128) {
                        memset(blepackdata.Data, 0, 128);
                        memcpy(blepackdata.Data, update_file,
                               (filelen - send_len) * sizeof(uint8_t));
                        pack_ble_to_json(&jsonpackdata, &blepackdata,
                                         (int) (filelen - send_len));
                        jsonpackdata.DataSize = (int) (filelen - send_len + 2);
                        send_len += (filelen - send_len);
                    } else {
                        memcpy(blepackdata.Data, &update_file[send_len],
                               128 * sizeof(uint8_t));
                        pack_ble_to_json(&jsonpackdata, &blepackdata, 128);
                        jsonpackdata.DataSize = 128 + 2;
                        send_len += 128;
                    }
                    //包序大于0x7F重置
                    if (blepackdata.PackNum > 0x7F) {
                        blepackdata.PackNum = 1;
                    }

                    send_ack_to_app(EM_STATUS_DEF);
                }
            }
        } else {
            if (step == 2) {
                LOGE("升级文件下载完毕，准备更新");
                memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
                send_ack_to_app(EM_STATUS_OK);
            }
        }
    } else {
        LOGE("无升级文件");
        memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
        send_ack_to_app(EM_STATUS_ERR);
        return 1;
    }
    return 0;
}

static int update_by_mf(void) {
    LOGE("进入更新");
    //更新超时时间
    timeout = time((time_t *) NULL);
    if(step == 2)
    {
        step = 0;
        memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
        memcpy(jsonpackdata.Data,"4000013000",10);
        jsonpackdata.DataSize = 5;
        send_ack_to_app(EM_STATUS_DEF);
    }
    else{
        if(step == 3)
        {
            step = 0;
            memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
            send_ack_to_app(EM_STATUS_OK);
            return 1;
        }
        else
        {
            jsonpackdata = thread_datapack[1];
            send_ack_to_app(EM_STATUS_DEF);
        }
    }

    return 0;
}

void *pthread_update_func(void *arg) {
    memset(&blepackdata, 0, sizeof(struct BlePackageData));
    memset(&jsonpackdata, 0, sizeof(struct SocketPackageData));
    pthread_t id = pthread_self();
    LOGE("线程1的id=%lu\n", id);
    //初始化超时时间
    timeout = time((time_t *) NULL);
    while (1) {
        switch (thread_datapack[1].SubType) {
            case EM_DOWNLODE_TO_APP:
                memset(update_file, 0, sizeof(update_file) / sizeof(update_file[0]));
                filelen = thread_datapack[1].DataSize;
                LOGE("filelen=%d\n", filelen);
                LOGE("filedata=%x\n", update_file[0]);
                memcpy(update_file, thread_datapack[1].Data, filelen * (sizeof(uint8_t)));
                send_ack_to_app(EM_STATUS_OK);
                thread_datapack[1].SubType = 100;
                break;
            case EM_DOWNLOCD_TO_MF:
                if (download_to_mf()) {
                    goto END;
                }
                break;
            case EM_MF_UPDATE:
                if (update_by_mf()) {
                    goto END;
                }
                break;
            default:
                //空闲，等待后续指令
                break;
        }
        if(wait_ack_from_mf())
        {
            break;
        }
    }
//        sleep(1);
    END:
    LOGE("线程1退出");
    pthread_exit(NULL);
}

void *pthread_reg_func(void *arg) {
//    char *csend;
    pthread_t id = pthread_self();
    LOGE("线程2的id=%lu\n", id);

//    while (1) {
//        if (thread_datapack[2].Type == 2) {
//            csend = send_json(thread_datapack[2]);
//            send(clientfd, csend, strlen(csend), 0);
//            memset(&thread_datapack[2], 0, sizeof(struct SocketPackageData));
//        }
//
//        sleep(1);
//    }
//
//    LOGE("线程2退出");
//    pthread_exit(NULL);
}