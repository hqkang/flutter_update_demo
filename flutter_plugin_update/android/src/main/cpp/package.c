#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include "cJSON.h"
#include "package.h"
#include "update.h"

#define READ_LEN (1024)
#define READ_MAX (1024*512)

char socket_rxbuff[READ_MAX];//socket接收数组
char socket_json_buff[READ_MAX];//接收到的json包

struct SocketPackageData rx_datapack;//临时接收数据包
struct SocketPackageData thread_datapack[5];//定义接收包
extern int clientfd;
extern pthread_t thread_id[5];

void StrToHex(uint8_t *pbDest, uint8_t *pbSrc, int nLen) {
    uint8_t h1, h2;
    uint8_t s1, s2;
    int i;
    for (i = 0; i < nLen; i++) {
        h1 = pbSrc[2 * i];
        h2 = pbSrc[2 * i + 1];

        s1 = h1 - 0x30;
        if (s1 > 9)
            s1 -= 7;
        s2 = h2 - 0x30;
        if (s2 > 9)
            s2 -= 7;
        pbDest[i] = s1 * 16 + s2;
    }
}

void HexToStr(uint8_t *pbDest, uint8_t *pbSrc, int nLen) {
    uint8_t ddl, ddh;
    int i;

    for (i = 0; i < nLen; i++) {
        ddh = 48 + pbSrc[i] / 16;
        ddl = 48 + pbSrc[i] % 16;
        if (ddh > 57) ddh = ddh + 7;
        if (ddl > 57) ddl = ddl + 7;
        pbDest[i * 2] = ddh;
        pbDest[i * 2 + 1] = ddl;
    }
    pbDest[nLen * 2] = '\0';
}

uint32_t get_return_state(uint8_t *code) {
    uint32_t state;
    state = (code[0] << 24) | (code[1] << 16) | (code[2] << 28) | (code[3] << 0);
    return state;
}

uint8_t strbuff[MAX_DATA];

static void receive_json_to_str(char *data) {
    cJSON *json;
    cJSON *item;


    memset(&rx_datapack, 0, sizeof(struct SocketPackageData));

    json = cJSON_Parse(data);
    if (!json) {
        LOGE("Error before: [%s]\n", cJSON_GetErrorPtr());
    } else {
        item = cJSON_GetObjectItem(json, "Type");
        rx_datapack.Type = item->valueint;
        item = cJSON_GetObjectItem(json, "SubType");
        rx_datapack.SubType = item->valueint;
        item = cJSON_GetObjectItem(json, "PackNum");
        rx_datapack.PackNum = item->valueint;
        item = cJSON_GetObjectItem(json, "Data");
        memcpy(strbuff, item->valuestring, strlen(item->valuestring));
        StrToHex(rx_datapack.Data, strbuff, (int) (strlen(item->valuestring) / 2));
        item = cJSON_GetObjectItem(json, "DataSize");
        rx_datapack.DataSize = item->valueint;
        item = cJSON_GetObjectItem(json, "Status");
        rx_datapack.Status = item->valueint;
    }
    cJSON_Delete(json);
}

static void analyze_packdata(void) {
    switch (rx_datapack.Type) {
        case EM_DOWNLODE:
            if (thread_id[1] == 0) {
                if (pthread_create(&thread_id[1], NULL, pthread_update_func, NULL) ==
                    0) {
                    LOGE("创建线程1成功");
                    //设置线程分离属性
                    pthread_detach(thread_id[1]);
                }
            }
            thread_datapack[1] = rx_datapack;
            break;
        case 6:
            if (thread_id[2] == 0) {
                if (pthread_create(&thread_id[2], NULL, pthread_reg_func, NULL) ==
                    0) {
                    LOGE("创建线程2成功");
                    //设置线程分离属性
                    pthread_detach(thread_id[2]);
                }
            }
            thread_datapack[2] = rx_datapack;
            break;
        default:
            break;
    }
}


char *send_json(struct SocketPackageData datapack) {
    cJSON *json = cJSON_CreateObject();
    char *out;

    cJSON_AddNumberToObject(json, "Type", datapack.Type);
    cJSON_AddNumberToObject(json, "SubType", datapack.SubType);
    cJSON_AddNumberToObject(json, "PackNum", datapack.PackNum);
    cJSON_AddStringToObject(json, "Data", datapack.Data);
    cJSON_AddNumberToObject(json, "DataSize", datapack.DataSize);
    cJSON_AddNumberToObject(json, "Status", datapack.Status);
    out = cJSON_PrintUnformatted(json);
    cJSON_Delete(json);
    return out;
}

void *socket_pthread_func(void *arg) {
    LOGE("接收线程创建成功!");
    clientfd = *(int *) arg;
    int rx_cnt;
    int rxbuf_len = 0;
    fd_set readfds;
    struct timeval timeout;  //select超时时间值
    int select_state; //接收返回值
    int timeout_cnt = 0;//超时计数
    free(arg);
    pthread_t id = pthread_self();
    LOGE("thread_id=%lu\n", id);
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(clientfd, &readfds);
        timeout.tv_sec = 10;  //超时
        timeout.tv_usec = 0;
        select_state = select(clientfd + 1, &readfds, NULL, NULL, &timeout);
        if (select_state > 0) {
            //是否产生了事件
            if (FD_ISSET(clientfd, &readfds)) {
                rx_cnt = recv(clientfd, &socket_rxbuff[rxbuf_len], READ_LEN, 0);
                if (rx_cnt == 0) {
                    LOGE("read empty,break");
                    break;
                }

                //心跳包处理
                if (socket_rxbuff[0] == 'A') {
                    send(clientfd, &socket_rxbuff[0], 1, 0);
                    timeout_cnt = 0;
                    rxbuf_len = 0;
                }

                if (socket_rxbuff[0] != '{') {
                    rxbuf_len = 0;
                } else {
                    rxbuf_len += rx_cnt;
                    if ((socket_rxbuff[rxbuf_len - 1]) != '}') {
                        //超出最大接收
                        if ((rxbuf_len + READ_LEN) > READ_MAX) {
                            LOGE("超出最大接收");
                            rxbuf_len = 0;
                        }
                    } else {
                        memset(socket_json_buff, 0,
                               sizeof(socket_json_buff) / sizeof(socket_json_buff[0]));
                        memcpy(socket_json_buff, socket_rxbuff, rxbuf_len);
                        //LOGE("json_buff=%s\n", json_buff);
                        receive_json_to_str(socket_json_buff);
                        rxbuf_len = 0;
                        analyze_packdata();
                    }
                }
            }
        } else if (select_state == 0) {
            timeout_cnt++;
            LOGE("Timeout!cnt=%d", timeout_cnt);
            if (timeout_cnt < 5) {
                continue;
            } else
                break;
        } else if (select_state < 0) {
            LOGE("Select error!");
            break;
        }
    }

    shutdown(clientfd, SHUT_RDWR);
    // close(clientfd);
    pthread_exit(NULL);
}
