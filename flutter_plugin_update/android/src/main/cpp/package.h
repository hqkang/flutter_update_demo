
#ifndef __PACKAGE__H
#define __PACKAGE__H

#include <android/log.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define  LOG_TAG    "socket_server"
#define  LOGI(FORMAT, ...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,FORMAT,##__VA_ARGS__)
#define  LOGE(FORMAT, ...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,FORMAT,##__VA_ARGS__)
#define  LOGD(FORMAT, ...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,FORMAT, ##__VA_ARGS__)

#define MAX_DATA (1024*500)    //每帧最大数据传输大小
#define MAX_PACK (1024*512)    //每帧最大包传输大小

#pragma pack(1)
//定义socket数据传输的结构体
struct SocketPackageData {
    int Type;                    //功能码
    int SubType;                 //子功能码
    int PackNum;                 //Package编号
    uint8_t Data[MAX_DATA];      //Package数据
    int DataSize;                //数据总大小
    int Status;                  //状态 0:默认值 1:完成 2:异常
};
#pragma pack()

enum Funstype {
    EM_DOWNLODE = 1,
};

enum Downlodesubtype {
    EM_DOWNLODE_TO_APP = 1,
    EM_DOWNLOCD_TO_MF = 2,
    EM_MF_UPDATE = 3,
};

typedef enum {
    EM_STATUS_DEF = 0,  //默认值
    EM_STATUS_OK = 1,   //完成
    EM_STATUS_ERR = 2,  //异常
} EM_UPDATE_STATUS;

extern char *send_json(struct SocketPackageData datapack);

extern void *socket_pthread_func(void *arg);

extern void HexToStr(uint8_t *pbDest, uint8_t *pbSrc, int nLen);

extern uint32_t get_return_state(uint8_t *code);

#ifdef __cplusplus
}
#endif

#endif
