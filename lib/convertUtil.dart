import 'dart:convert';
import 'dart:typed_data';
import 'package:convert_hex/convert_hex.dart';

// 进制转换工具
class ConvertUtil {
  // 将List十进制数组转换成十六进制字符串
  String listTo16String(List result){
    String s = "";
    for (int i in result) {
      if (i == 0) {
        s = "$s${"00"}";
      } else {
        if (Hex.encode(i).length == 1) {
          s = "${s}0${Hex.encode(i)}";
          //          print(s);
        } else {
          s = "$s${Hex.encode(i)}";
        }
      }
    }
    return s;
  }

  // 将十六进制字符串转换成List十进制数组
  Uint8List stringToList(String result){
    List<int> tempArr = List();
    int index = 0;
    String tempStr = '';
    for (var i = 0; i < result.length;i++){
      if (index != 2){
        tempStr = tempStr + result[i];
        index++;
      }
      if (index == 2){
        tempArr.add(Hex.decode(tempStr));
        index = 0;
        tempStr = '';
      }
    }
    return Uint8List.fromList(tempArr);
  }

  // 将十六进制字符串转换成十进制数
  int stringToInt(String result){
    return Hex.decode(result);
  }

  // 将十进制数转换成十六进制字符串
  String intTo16String(int result){
    return Hex.encode(result);
  }

  // 将ascii码数组换成字符串
  String asciiListToString(List<int> result){
    return ascii.decode(result);
  }

  // 将字符串换成ascii Uint8List数组
  Uint8List asciiStringToUint8List(String result){
    return ascii.encode(result);
  }

  // 将钥匙后三位数组异或出一个新的钥匙后3位数组
  List makeNewKey2(List key3List){
    List newList = List();
    if (key3List.length == 1){
      newList.add(key3List[0][0]);
      newList.add(key3List[0][1]);
      newList.add(key3List[0][2]+1);
    }else{
      int temp1 = key3List[0][0];
      int temp2 = key3List[0][1];
      int temp3 = key3List[0][2];
      for (var i = 1; i < key3List.length; i++){
        temp1 = temp1^key3List[i][0];
        temp2 = temp2^key3List[i][1];
        temp3 = temp3^key3List[i][2];
      }
      newList.add(temp1);
      newList.add(temp2);
      newList.add(temp3);
    }
    return newList;
  }

  // 钥匙最后一位数组异或出一个新的
  int makeLastNewKey(List key3List){
    int newKey;
    if (key3List.length == 1){
      newKey = key3List[0]+1;
    }else{
      int temp = key3List[0];
      for (var i = 1; i < key3List.length; i++){
        temp = temp^key3List[i];
      }
      newKey = temp;
    }
    return newKey;
  }
}