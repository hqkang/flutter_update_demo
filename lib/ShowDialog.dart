//import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_carctrl/routes/CustomNavigatorObserver.dart';
import 'package:flutter_demo/RouterKey.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class ShowDialog extends Dialog{
  //注册成功
  registerSuccessDialog(BuildContext context, VoidCallback Ok) async {
    await showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        builder: (context) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 260,
                  height: 260,
                  //color: Colors.white,
                  decoration:BoxDecoration(
                      color: Colors.white,
                      // border: Border.all(
                      //   color: Colors.white,
                      //   width:2.0,
                      // ),
                      borderRadius: BorderRadius.all(Radius.circular(15.0))
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 30),
                      Icon(Icons.check_circle,color: Color(0xFF4E96FD),size: 50),
                      SizedBox(height: 20),
                      Text("您已注册成功",style: TextStyle(fontSize: 16, color: Colors.black, decoration: TextDecoration.none)),
                      SizedBox(height: 10),
                      Text("欢迎加入澳多大家庭！",style: TextStyle(fontSize: 16, color: Colors.grey, decoration: TextDecoration.none)),
                      Expanded(
                        flex: 1,
                        child: SizedBox(height: 1),
                      ),
                      SizedBox(
                        width: 220,
                        child: RaisedButton(
                          color: Color(0xFF4E96FD),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)
                          ),
                          child: Text("去登陆体验吧",style: TextStyle(fontSize: 16,color: Colors.white)),
                          onPressed: Ok,
                        ),
                      ),
                      SizedBox(height: 30),
                    ],
                  ),
                ),
                SizedBox(height: 40),
                Container(
                    width: 200,
                    height: 50,
                    color: Colors.transparent,
                    child: GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Icon(Icons.cancel, color: Colors.white, size: 30)
                    )
                ),
              ],
            ),
          );
        }
    );
  }

  //重置密码
  changPwdSuccessDialog(BuildContext context, VoidCallback Ok) async {
    await showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        builder: (context) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 290,
                  height: 330,
                  //color: Colors.white,
                  decoration:BoxDecoration(
                      color: Colors.white,
                      // border: Border.all(
                      //   color: Colors.white,
                      //   width:2.0,
                      // ),
                      borderRadius: BorderRadius.all(Radius.circular(15.0))
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 27),
                      //Icon(Icons.check_circle,color: Color(0xFF4E96FD),size: 50),
                      Image.asset("drawable/changepwd.png",height: 150),
                      SizedBox(height: 22),
                      Text("您已成功修改密码",style: TextStyle(fontSize: 16, color: Color(0xFF4C5268), decoration: TextDecoration.none)),
                      Expanded(
                        flex: 1,
                        child: SizedBox(height: 1),
                      ),
                      SizedBox(
                        width: 220,
                        child: RaisedButton(
                          color: Color(0xFF4E96FD),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)
                          ),
                          child: Text("重新登录",style: TextStyle(fontSize: 16,color: Colors.white)),
                          onPressed: Ok,
                        ),
                      ),
                      SizedBox(height: 30),
                    ],
                  ),
                ),
                SizedBox(height: 35),
                Container(
                    width: 200,
                    height: 50,
                    color: Colors.transparent,
                    child: GestureDetector(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child: Icon(Icons.cancel, color: Colors.white, size: 30)
                    )
                ),
              ],
            ),
          );
        }
    );
  }

  //控车弹框
  carControlmodelBottomSheet(BuildContext context, String title, String title_2, Widget body, String title_3, String title_4, VoidCallback callback, VoidCallback callback2) async{
    await showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,//showModalBottomSheet默认的背景颜色为白色，这时需要将showModalBottomSheet的背景颜色设置为透明
        builder: (context) {
          return Container(
              height: 466.0,
              decoration:BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 5),
                    SizedBox(
                      height: 40,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Stack(
                                children: <Widget>[
                                  Align(
                                      alignment:AlignmentDirectional.center,
                                      child: Text(title,style: TextStyle(fontSize: 18, color: Color(0xFF4C5268), decoration: TextDecoration.none))
                                  ),
                                  Align(
                                    alignment:AlignmentDirectional.centerEnd,
                                    child: IconButton(
                                      icon: Icon(Icons.close, color: Color(0xFFC4C4C4), size: 20),
                                      onPressed: (){
                                        Navigator.pop(context);
                                      },
                                    ),
                                  )
                                ],
                              )
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                        width: 200,
                        height: 40,
                        child: Text(title_2,style: TextStyle(fontSize: 14, color: Color(0xFFC4C4C4), decoration: TextDecoration.none), textAlign: TextAlign.center)
                    ),
                    //SizedBox(height: 10),


                    Expanded(
                        flex: 2,
                        child: SizedBox(height: 1)
                    ),

                    ////控车弹框中间需要变动的部分
                    body,

                    Expanded(
                        flex: 1,
                        child: SizedBox(height: 1)
                    ),

                    SizedBox(height: 10),
                    Row(
                      children: <Widget>[
                        SizedBox(width: 15),
                        Expanded(
                          flex: 1,
                          child: RaisedButton(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Color(0xFF7EB1FA)),
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: Text(title_3, style: TextStyle(fontSize: 18,color: Color(0xFF7EB1FA))),
                              onPressed: callback
                          ),
                        ),
                        SizedBox(width: 15),
                        Expanded(
                          flex: 1,
                          child: RaisedButton(
                              color: Color(0xFF7EB1FA),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: Text(title_4, style: TextStyle(fontSize: 18,color: Colors.white)),
                              onPressed: callback2
                          ),
                        ),
                        SizedBox(width: 15),
                      ],
                    ),
                    SizedBox(height: 15),
                  ]
              )
          );
        }
    );
  }

  //单按钮控车弹框
  carControlOneButtonModelBottomSheet(BuildContext context, String title, String title_2, Widget body, Widget title_3, VoidCallback callback2) async{
    await showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,//showModalBottomSheet默认的背景颜色为白色，这时需要将showModalBottomSheet的背景颜色设置为透明
        builder: (context) {
          return Container(
              height: 466.0,
              decoration:BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10)),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 5),
                    SizedBox(
                      height: 40,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: Stack(
                                children: <Widget>[
                                  Align(
                                      alignment:AlignmentDirectional.center,
                                      child: Text(title,style: TextStyle(fontSize: 18, color: Color(0xFF4C5268), decoration: TextDecoration.none))
                                  ),
                                  Align(
                                    alignment:AlignmentDirectional.centerEnd,
                                    child: IconButton(
                                      icon: Icon(Icons.close, color: Color(0xFFC4C4C4), size: 20),
                                      onPressed: (){
                                        Navigator.pop(context);
                                      },
                                    ),
                                  )
                                ],
                              )
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                        width: 200,
                        height: 40,
                        child: Text(title_2,style: TextStyle(fontSize: 14, color: Color(0xFFC4C4C4), decoration: TextDecoration.none), textAlign: TextAlign.center)
                    ),

                    Expanded(
                        flex: 2,
                        child: SizedBox(height: 1)
                    ),

                    ////控车弹框中间需要变动的部分
                    body,

                    Expanded(
                        flex: 1,
                        child: SizedBox(height: 1)
                    ),

                    SizedBox(height: 10),
                    Row(
                      children: <Widget>[
                        SizedBox(width: 50),
                        Expanded(
                          flex: 1,
                          child: RaisedButton(
                              color: Color(0xFF7EB1FA),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: title_3,
                              onPressed: callback2
                          ),
                        ),
                        SizedBox(width: 50),
                      ],
                    ),
                    SizedBox(height: 15),
                  ]
              )
          );
        }
    );
  }

  //标题栏更多 普通
  Future<String> ordinaryMoreDialog(BuildContext context) async {
    String result = await showMenu(
      //barrierDismissible: true,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        position: RelativeRect.fromLTRB(30, 75, 100, 100),
        items: <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
              value: 'maincar',
              child: Row(
                children: <Widget>[
                  Image.asset("drawable/icon_replace.png",width: 12),
                  SizedBox(width: 5),
                  Text("更换车辆图片", style: TextStyle(color: Color(0xFFC4C4C4), fontSize: 14, decoration:TextDecoration.none)),
                ],
              )
          ),
        ]
    );
    //print("moreDialog===$result");
    return result;
  }

  //标题栏更多 测试
  Future<String> testMoreDialog(BuildContext context) async {
    String result = await showMenu(
      //barrierDismissible: true,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        position: RelativeRect.fromLTRB(30, 75, 100, 100),
        items: <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
              value: 'maincar',
              child: Row(
                children: <Widget>[
                  Image.asset("drawable/icon_replace.png",width: 12),
                  SizedBox(width: 5),
                  Text("更换车辆图片", style: TextStyle(color: Color(0xFFC4C4C4), fontSize: 14, decoration:TextDecoration.none)),
                ],
              )
          ),
          PopupMenuDivider(height: 1.0),
          PopupMenuItem<String>(
              value: 'scan',
              child: Row(
                children: <Widget>[
                  Image.asset("drawable/icon_scan.png",width: 12),
                  SizedBox(width: 5),
                  Text('扫描', style: TextStyle(color: Color(0xFFC4C4C4), fontSize: 14, decoration:TextDecoration.none)),
                ],
              )
          )
        ]
    );
    //print("moreDialog===$result");
    return result;
  }

  //一般提示弹框
  tipsDialog(BuildContext context, double height, double textBoxHeight, String text, VoidCallback ok) async {
    await showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: context,
        builder: (context) {
          return Center(
            child: Container(
              width: 320,
              height: height,
              //color: Colors.white,
              decoration:BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(
                  //   color: Colors.white,
                  //   width:2.0,
                  // ),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
              ),
              child: Column(
                children: <Widget>[
                  Container(
                      height: textBoxHeight,
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: Center(child: Text(text, style: TextStyle(fontSize: 16, color: Colors.black, decoration: TextDecoration.none), textAlign: TextAlign.center))
                  ),
                  //Divider(),
                  SizedBox(height: 1,child: Divider()),
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          child: FlatButton(
                              child: Text("取消",style: TextStyle(fontSize: 16,color: Colors.black)),
                              onPressed: () {
                                Navigator.pop(context);
                              }
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        child: VerticalDivider(),
                      ),
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          child: FlatButton(
                              child: Text("确认",style: TextStyle(fontSize: 16,color: Colors.black)),
                              onPressed: ok
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  //一般提示弹框
  alertDialog(String text) async {
    await showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: RouterKey.navigatorKey.currentContext,
        builder: (context) {
          return Center(
              child: Container(
                  width: 280,
                  height: 180,
                  padding: EdgeInsets.fromLTRB(5, 20, 5, 5),
                  decoration:BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))
                  ),
                  child: Column(
                    children: [
                      Expanded(
                          flex:1,
                          child: Center(
                              child: Text(text, style: TextStyle(fontSize: 14, color: Colors.black, decoration: TextDecoration.none), textAlign: TextAlign.center)
                          )
                      ),
                      Divider(),
                      SizedBox(
                        width: 280,
                        height: 45,
                        child: FlatButton(
                          child: Text("关闭"),
                          onPressed: (){
                            RouterKey.navigatorKey.currentState.pop();
                          },
                        ),
                      )

                    ],
                  )
              )
          );
        }
    );
  }

}