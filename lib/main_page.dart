import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_demo/BluetoothConnection.dart';
import 'package:flutter_demo/custom_dialog.dart';
import 'package:flutter_demo/dio_utils.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

const debug = true;
String fileName = 'update';
class MainPage extends StatefulWidget {
  // This widget is the root of your application.
  _MainPageState createState() => _MainPageState();
}
class _MainPageState extends State<MainPage>{
  BluetoothConnection bluetoothConnection = BluetoothConnection.blueToothSingleton();
  static ProgressDialog pr;
  int downloadCount = 0;
  List urlList = List();
  List fileDataList = List();
  File file;
  String versionString = '';

  @override
  void initState() {
    super.initState();
    _bindBackgroundIsolate();
    // initDownloader();
  }

  // initDownloader() async{
  //   await FlutterDownloader.initialize(debug: debug);
  // }

  ReceivePort _port = ReceivePort();
  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send');
  }

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      if (debug) {
        print('UI Isolate Callback: $data');
      }
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      setState(() {
        print('test------');
        if (status == DownloadTaskStatus.running) {
          pr.update(progress: progress.toDouble(), message: '正在下载, 请稍等...');
        }
        if (status == DownloadTaskStatus.failed) {
          downloadCount = 0;
          fileDataList = List();
          // ShowCustomTwoButtonDialog(context: context, title: '提示', height: 160, colors: Colors.black54, content: '下载出错,是否重新下载', buttonTitle: '重新下载', onTap: (){
          //   Navigator.pop(context);
          //   for (var i = 0; i < urlList.length; i++){
          //     doUpdate(context, urlList[i], '${fileName}${i}.ktr');
          //   }
          // }).tipsDialog();
          if (pr.isShowing()) {
            pr.hide();
          }
        }
        if (status == DownloadTaskStatus.complete) {
          openFile('${fileName}${downloadCount}.ktr');
          downloadCount++;
          if (downloadCount == urlList.length){
            if (pr.isShowing()) {
              pr.hide();
            }
            ShowCustomTwoButtonDialog(context: context, title: '提示', colors: Colors.black54, content: '文件已下载', height: 160, buttonTitle: '立即安装', onTap: (){
              Navigator.pop(context);
              bluetoothConnection.showWaitDialogStart2('正在更新...');
              // bluetoothConnection.packSend();
            },).tipsDialog();
          }else{
            if (pr.isShowing()) {
              pr.hide();
            }
            ShowCustomTwoButtonDialog(context: context, title: '提示', colors: Colors.black54, content: '文件已下载', height: 160, buttonTitle: '立即安装', onTap: (){
              Navigator.pop(context);
              bluetoothConnection.showWaitDialogStart2('正在更新...');
              // bluetoothConnection.packSend();
            },).tipsDialog();
          }


          // List<Uint8List> list = await file.readAsBytes();
        }
      });

    });
  }

  // 获取file存放地址(外部路径)
  Future<String> get _fileLocalPath async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  ///3.执行更新操作
  doUpdate(BuildContext context, String url, String fileName) async {
    print('filename: ${fileName}');
    //关闭更新内容提示框
    // Navigator.pop(context);
    //获取权限
    var per = await checkPermission();
    if(per != null && !per){
      print('无权限-------');
      return null;
    }
    //开始下载file
    await executeDownload(context , url, fileName);
  }

  ///4.检查是否有权限
  Future<bool> checkPermission() async {
    //检查是否已有读写内存权限
    PermissionStatus status = await Permission.storage.status;
    print('status--------${status}');

    //判断如果还没拥有读写权限就申请获取权限
    if(status != PermissionStatus.granted){
      var map = await Permission.storage.request();
      print('map: ${map}');
      if(map != PermissionStatus.granted){
        return false;
      }
    }
    return true;
  }

  ///5.下载file
  Future<void> executeDownload(BuildContext context ,String url, String fileName) async {
    //下载时显示下载进度dialog
    if (pr == null){
      pr = new ProgressDialog(context,type: ProgressDialogType.Download, isDismissible: false, showLogs: true);
      if (!pr.isShowing()) {
        pr.show();
      }
    }
    //file存放路径
    final path = await _fileLocalPath;
    print('path: ${path}');
    file = File(path + '/' + fileName);
    print('filepath: ${path + '/' + fileName}');
    // if (await file.exists()) await file.delete();

    FlutterDownloader.registerCallback(downloadCallback);
    //下载
    final taskId = await FlutterDownloader.enqueue(
        url: url,//下载最新file的网络地址
        fileName: fileName,
        savedDir: path,
        showNotification: true,
        openFileFromNotification: true);
  }

  //6.更新
  Future<Null> openFile(String fileName) async {
    String path = await _fileLocalPath;
    print('file地址: ${path + '/' + fileName}');
    File file = File(path + '/' + fileName);
    List list = await file.readAsBytes();
    fileDataList.add(list);
    // Log.e('file地址: ${path + '/' + fileName}-----list: ${list}-----list.length: ${list.length}');
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    if (debug) {
      print(
          'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    }
    final SendPort send =
    IsolateNameServer.lookupPortByName('downloader_send');
    send.send([id, status, progress]);
    print('id: ${id}--------status: ${status}-------progress: ${progress}---pr: ${pr}');
  }

  Future checkChipVersion() async {
    // final Map<String, String> params = <String, String>{};
    // params['timestamp'] = Utils.getTimestamp();
    // params['nonce'] = Utils.getRandomCharParams();
    // Utils.handleRequestParams(params);
    EasyLoading.show(status: 'loading...');
    await DioUtils.instance.requestNetwork(
        Method.get, '',
        onSuccess: (data){
          EasyLoading.dismiss();
          if (data['status'] == 200){
            print('data===${data['data']}');
            // 主芯片boot版本
            String boot = data['data']['data']['boot'];
            // 主芯片boot版本URL
            String boot_url = data['data']['data']['boot_url'];
            // 芯片1boot版本
            String boot1 = data['data']['data']['boot1'];
            // 芯片1boot版本URL
            String boot1_url = data['data']['data']['boot1_url'];
            // 芯片2boot版本
            String boot2 = data['data']['data']['boot2'];
            // 芯片2boot版本URL
            String boot2_url = data['data']['data']['boot2_url'];
            // 主芯片APP版本
            String app = data['data']['data']['app'];
            // 主芯片APP版本URL
            String app_url = data['data']['data']['app_url'];
            // 芯片1APP版本
            String app1 = data['data']['data']['app1'];
            // 芯片1APP版本URL
            String app1_url = data['data']['data']['app1_url'];
            // 芯片2APP版本
            String app2 = data['data']['data']['app2'];
            // 芯片2APP版本URL
            String app2_url = data['data']['data']['app2_url'];
            urlList = List();


            if (urlList.length > 0){
              print('有${urlList.length}个新版本-----链接: ${urlList}');
              ShowCustomTwoButtonDialog(context: context, title: '提示', height: 160, colors: Colors.black54, content: '有新版本,是否立即更新', buttonTitle: '更新', onTap: (){
                Navigator.pop(context);
                fileDataList = List();
                doUpdate(context, urlList[0], '${fileName}${0}.ktr');
                // for (var i = 0; i < urlList.length; i++){
                //   doUpdate(context, urlList[i], '${fileName}${i}.ktr');
                // }
              }).tipsDialog();
            }else{
              Fluttertoast.showToast(msg: '当前版本为最新版本');
            }
          }else if(data['status'] == 410000){

          }else{
            Fluttertoast.showToast(msg: data['msg']);
          }
          setState(() {
          });
        },
        onError: (code,message){
          EasyLoading.dismiss();
          Fluttertoast.showToast(msg: message);
          setState(() {
          });
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //修改颜色
        ),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        title: new Text(
          '更新',
          style: TextStyle(
              color: Colors.black87
          ),
        ),
        centerTitle: true,
      ),
      body: FlutterEasyLoading(
        child: Flex(
          direction: Axis.vertical,
          children: <Widget>[
            Container(
              height: 50,
              child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          versionString
                      )
                    ],
                  ),
                  onPressed: (){

                  }
              ),
            ),
            SizedBox(height: 1,child: Divider()),
            Container(
              height: 50,
              child: FlatButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                        '检查固件版本'
                    )
                  ],
                ),
                onPressed: (){
                  checkChipVersion();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}