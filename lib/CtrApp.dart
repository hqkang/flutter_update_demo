import 'dart:async';
import 'dart:convert';
import 'package:flustars/flustars.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_demo/SharedPreferencesUtil.dart';
import 'package:flutter_demo/common.dart';
import 'package:flutter_demo/ShowDialog.dart';
import 'package:flutter_demo/RouterKey.dart';
import 'package:flutter_demo/BluetoothConnection.dart';
import 'package:flutter_demo/SharedPreferencesUtil.dart';
//import 'package:web_socket_channel/status.dart' as status;

class CtrApp with ChangeNotifier{//, DiagnosticableTreeMixin {

  String _websocketadd = "wss://iov.edaoduo.com/aoduo/test/ws/";
  //String _h5url = "https://iov.edaoduo.com/aoduo/prod/web/h5/html/";
  String _h5url = "http://aoduo.test.edaoduo.com/h5/html/";
  String _h5maps = "http://aoduo.test.edaoduo.com/h5_maps/html/";

  String get h5url => _h5url;
  String get h5maps => _h5maps;

  int _userId = 0;
  int _memberPoints = 0;
  int _carid = 0;
  int _online=0;
  int _engine=0;
  int _lockstate=0;
  int _windowstate=0;
  int _doorstate=0;
  int _trunkstate=0;
  int _notice=0;
  int _maxwin=8;
  int _devtype = 0;
  int _userAuth = 0;
  int _isconnect = 0;
  int _isOpenBlue = 0;
  int _isAutoConnect = 0;

  String _vipAccount = ''; // 会员账户
  String _userName = 'MOFIA001'; // 用户名
  String _portrait = 'assets/images/me/default_portrait.png'; // 用户头像
  String _operationType = '';
  String _confirmVin = '';
  String _carSystem = ''; // 系统
  String _vag = '';
  String _systemVersion = '';
  String _vin = '';
  String _keyCount = '';
  String _currentKeyId = '';
  String _currentReadKeyId = '';
  String _waitWriteKeyId = '';
  String _loginName = '';
  int _currentKeyIndex = 0;
  bool _isUpdateSuccess = false;
  bool _getChipVersion = false;
  int _currentSelectedKeyIndex = -1;
  int _readCarDataCount = 0;
  int _currentUpdateIndex = -1;
  int _requestFailedCount = 0;
  String _fileString = '';
  String _carKey1 = '';
  String _carKey2 = 'FFFFFFFF';
  String _carKey3 = 'FFFFFFFF';
  String _carKey4 = 'FFFFFFFF';
  String _carKey5 = 'FFFFFFFF';
  String _carKey6 = 'FFFFFFFF';
  String _carKey7 = 'FFFFFFFF';
  String _carKey8 = 'FFFFFFFF';
  String _keyType = '';
  String _readKeyId = '';
  String _writeKeyId = '';
  String _readKeySn = '';
  String _readKeyDistinguish = '';
  String _readKeyInfo = '';
  String _authFailInfo = '';
  String _androidDeviceId = '';
  String _macString = '';
  String _signString = '';
  int _systemId = 0;
  int _comfortableCarTypeId = 0;
  int _matchTypeId = 0;
  int _operationId = 0;
  StreamSubscription<Map<String, Object>> _locationListener;
  String _hasDialogShow = '1';
  List   _readKeyData = List();
  List   _learnKeyList = List();
  List   _originKeyList = List();
  List   _key3List = List();
  List   _lastKeyList = List();
  List _baseKeyIdList = List();
  List _writeKeyIdList = List();
  List _mqbWriteKeyIdList = List();
  List _writeKeyDataList = List();
  String _mainChipBootVersion = '';
  String _mainChipAppVersion = '';
  String _chip1BootVersion = '';
  String _chip1AppVersion = '';
  String _chip2BootVersion = '';
  String _chip2AppVersion = '';
  bool _isStartUpdate = false;
  bool _isLastPackage = false;
  List _fileDataList = List();
  List _originFileDataList = List();
  bool _openEngine = false;
  bool _openIgnition = false;
  bool _uploadFailed = false;
  bool _isOpenGps = false;
  String _learnKeyCount = '';
  int _isSpecialMatchType = 0;
  String _nickName = '';
  String _authRealName = '';
  int _isAuth = 0;
  int _currentWriteKeyIndex = 0;
  ScanResult _currentBle;
  String _requestId = '';
  String _authAddress = '';
  String _authPhone = '';
  String _businessPic = '';
  String _IDCardPic = '';
  String _authQQ = '';
  String _authSn = '';
  String _qq = '';
  String _email = '';
  String _address = '';
  String _readInfoErr = '0';
  String _currentOperation = '';
  String _clientmobile = "";
  String _userLat = '';
  String _userLng = '';
  String _userAddress = '';
  BluetoothDevice _bluetoothDevice;
  //String _ownerphone = "";
  String  _carno = "未选中车辆";
  String _uptime = "";
  String _mileage = "";
  String _devname = "";
  String _brandAndModel = "";
  String _oil = "";
  String _bat = "";
  String _careditcarid = "0";
  String _careditdevid = "0";
  String _lendid = "0";

  double _temp = 16.0;

  bool _borrowstart = false;
  bool _borrowlock = false;
  bool _borrowlocation = false;
  bool _borrowtrace = false;

  void cleanAll(){
    _userId = 0;
    _memberPoints = 0;
    _vipAccount = '';
    _userName = '';
    _portrait = 'assets/images/me/default_portrait.png';
    _operationType = '';
    _confirmVin = '';
    _carSystem = ''; // 系统
    _vag = '';
    _systemVersion = '';
    _vin = '';
    _keyCount = '';
    _currentKeyId = '';
    _currentReadKeyId = '';
    _waitWriteKeyId = '';
    _loginName = '';
    _currentKeyIndex = 0;
    _currentSelectedKeyIndex = -1;
    _readCarDataCount = 0;
    _currentUpdateIndex = -1;
    _requestFailedCount = 0;
    _isOpenGps = false;
    _carKey1 = '';
    _carKey2 = '';
    _carKey3 = 'FFFFFFFF';
    _carKey4 = 'FFFFFFFF';
    _carKey5 = 'FFFFFFFF';
    _carKey6 = 'FFFFFFFF';
    _carKey7 = 'FFFFFFFF';
    _carKey8 = 'FFFFFFFF';
    _keyType = '';
    _readKeyId = '';
    _writeKeyId = '';
    _readKeySn = '';
    _readKeyDistinguish = '';
    _readKeyInfo = '';
    _authFailInfo = '';
    _hasDialogShow = '1';
    _readKeyData = List();
    _learnKeyList = List();
    _originKeyList = List();
    _key3List = List();
    _lastKeyList = List();
    _baseKeyIdList = List();
    _writeKeyIdList = List();
    _mqbWriteKeyIdList = List();
    _writeKeyDataList = List();
    _mainChipBootVersion = '';
    _mainChipAppVersion = '';
    _chip1BootVersion = '';
    _chip1AppVersion = '';
    _chip2BootVersion = '';
    _chip2AppVersion = '';
    _openEngine = false;
    _isUpdateSuccess = false;
    _getChipVersion = false;
    _openIgnition = false;
    _uploadFailed = false;
    _learnKeyCount = '';
    _nickName = '';
    _authRealName = '';
    _isAuth = 0;
    _isSpecialMatchType = 0;
    _currentWriteKeyIndex = 0;
    _requestId = '';
    _authAddress = '';
    _authPhone = '';
    _businessPic = '';
    _IDCardPic = '';
    _authQQ = '';
    _authSn = '';
    _qq = '';
    _email = '';
    _address = '';
    _readInfoErr = '0';
    _currentOperation = '';
    _clientmobile = "";
    _userLat = '';
    _userLng = '';
    _userAddress = '';
  }

  int get userId => _userId;
  void setUserid(int userId) {
    _userId = userId;
    //保存
    SharedPreferencesUtil.saveData<int>("userId", userId);
    notifyListeners();
  }

  int get memberPoints => _memberPoints;
  void setMemberPoints(int memberPoints) {
    _memberPoints = memberPoints;
    //保存
    SharedPreferencesUtil.saveData<int>("memberPoints", memberPoints);
    notifyListeners();
  }

  int get carid => _carid;
  void setCarid(int carid) {
    _carid = carid;
    //保存
    SharedPreferencesUtil.saveData<int>("carid", carid);
    notifyListeners();
  }

  int get online => _online;
  void setOnline(int online) {
    _online = online;
    notifyListeners();
  }

  int get engine => _engine;
  void setEngine(int engine) {
    _engine = engine;
    notifyListeners();
  }

  int get lockstate => _lockstate;
  void setLockstate(int lockstate) {
    _lockstate = lockstate;
    notifyListeners();
  }

  int get windowstate => _windowstate;
  void setWindowstate(int windowstate) {
    _windowstate = windowstate;
    notifyListeners();
  }

  int get doorstate => _doorstate;
  void setDoorstate(int doorstate) {
    _doorstate = doorstate;
    notifyListeners();
  }

  int get trunkstate => _trunkstate;
  void setTrunkstate(int trunkstate) {
    _trunkstate = trunkstate;
    notifyListeners();
  }

  int get notice => _notice;
  void setNotice(int notice) {
    _notice = notice;
    notifyListeners();
  }

  int get maxwin => _maxwin;
  void setMaxwin(int maxwin) {
    _maxwin = maxwin;
    notifyListeners();
  }

  int get devtype => _devtype;
  void setDevtype(int devtype) {
    _devtype = devtype;
    notifyListeners();
  }

  int get userAuth => _userAuth;
  void setUserAuth(int userAuth) {
    _userAuth = userAuth;
    notifyListeners();
  }

  int get isconnect => _isconnect;
  void setIsconnect(int isconnect) {
    _isconnect = isconnect;
    notifyListeners();
  }

  int get isOpenBlue => _isOpenBlue;
  void setIsOpenBlue(int isOpenBlue) {
    _isOpenBlue = isOpenBlue;
    notifyListeners();
  }

  int get isAutoConnect => _isAutoConnect;
  void setIsAutoConnect(int isAutoConnect) {
    _isAutoConnect = isAutoConnect;
    notifyListeners();
  }


  String get vipAccount => _vipAccount;
  void setVipAccount(String vipAccount) {
    _vipAccount = vipAccount;
    //保存
    SharedPreferencesUtil.saveData<String>("vipAccount", vipAccount);
    notifyListeners();
  }

  String get userName => _userName;
  void setUserName(String userName) {
    _userName = userName;
    //保存
    SharedPreferencesUtil.saveData<String>("userName", userName);
    notifyListeners();
  }

  String get portrait => _portrait;
  void setPortrait(String portrait) {
    _portrait = portrait;
    //保存
    SharedPreferencesUtil.saveData<String>("portrait", portrait);
    notifyListeners();
  }

  String get nickName => _nickName;
  void setNickName(String nickName) {
    _nickName = nickName;
    //保存
    SharedPreferencesUtil.saveData<String>("nickName", nickName);
    notifyListeners();
  }

  String get authRealName => _authRealName;
  void setAuthRealName(String authRealName) {
    _authRealName = authRealName;
    //保存
    SharedPreferencesUtil.saveData<String>("authRealName", _authRealName);
    notifyListeners();
  }

  String get requestId => _requestId;
  void setRequestId(String requestId) {
    _requestId = requestId;
    //保存
    SharedPreferencesUtil.saveData<String>("requestId", _requestId);
    notifyListeners();
  }

  ScanResult get currentBle => _currentBle;
  void setCurrentBle(ScanResult currentBle) {
    _currentBle = currentBle;
    //保存
    SharedPreferencesUtil.saveData<ScanResult>("currentBle", _currentBle);
    notifyListeners();
  }

  String get userLat => _userLat;
  void setUserLat(String userLat) {
    _userLat = userLat;
    //保存
    SharedPreferencesUtil.saveData<String>("userLat", _userLat);
    notifyListeners();
  }

  String get userLng => _userLng;
  void setUserLng(String userLng) {
    _userLng = userLng;
    //保存
    SharedPreferencesUtil.saveData<String>("userLng", _userLng);
    notifyListeners();
  }

  String get userAddress => _userAddress;
  void setUserAddress(String userAddress) {
    _userAddress = userAddress;
    //保存
    SharedPreferencesUtil.saveData<String>("userAddress", _userAddress);
    notifyListeners();
  }

  int get isAuth => _isAuth;
  void setIsAuth(int isAuth) {
    _isAuth = isAuth;
    //保存
    SharedPreferencesUtil.saveData<int>("isAuth", isAuth);
    notifyListeners();
  }

  int get isSpecialMatchType => _isSpecialMatchType;
  void setIsSpecialMatchType(int isSpecialMatchType) {
    _isSpecialMatchType = isSpecialMatchType;
    //保存
    SharedPreferencesUtil.saveData<int>("isSpecialMatchType", _isSpecialMatchType);
    notifyListeners();
  }

  int get currentWriteKeyIndex => _currentWriteKeyIndex;
  void setCurrentWriteKeyIndex(int currentWriteKeyIndex) {
    _currentWriteKeyIndex = currentWriteKeyIndex;
    //保存
    SharedPreferencesUtil.saveData<int>("currentWriteKeyIndex", _currentWriteKeyIndex);
    notifyListeners();
  }

  String get authAddress => _authAddress;
  void setAuthAddress(String authAddress) {
    _authAddress = authAddress;
    //保存
    SharedPreferencesUtil.saveData<String>("authAddress", _authAddress);
    notifyListeners();
  }

  String get authPhone => _authPhone;
  void setAuthPhone(String authPhone) {
    _authPhone = authPhone;
    //保存
    SharedPreferencesUtil.saveData<String>("authPhone", _authPhone);
    notifyListeners();
  }

  String get authQQ => _authQQ;
  void setAuthQQ(String authQQ) {
    _authQQ = authQQ;
    //保存
    SharedPreferencesUtil.saveData<String>("authQQ", _authQQ);
    notifyListeners();
  }

  String get authSn => _authSn;
  void setAuthSn(String authSn) {
    _authSn = authSn;
    //保存
    SharedPreferencesUtil.saveData<String>("authSn", _authSn);
    notifyListeners();
  }

  String get businessPic => _businessPic;
  void setBusinessPic(String businessPic) {
    _businessPic = businessPic;
    //保存
    SharedPreferencesUtil.saveData<String>("businessPic", businessPic);
    notifyListeners();
  }

  String get IDCardPic => _IDCardPic;
  void setIDCardPic(String IDCardPic) {
    _IDCardPic = IDCardPic;
    //保存
    SharedPreferencesUtil.saveData<String>("IDCardPic", _IDCardPic);
    notifyListeners();
  }

  String get qq => _qq;
  void setQq(String qq) {
    _qq = qq;
    //保存
    SharedPreferencesUtil.saveData<String>("qq", qq);
    notifyListeners();
  }

  String get email => _email;
  void setEmail(String email) {
    _email = email;
    //保存
    SharedPreferencesUtil.saveData<String>("email", email);
    notifyListeners();
  }

  String get address => _address;
  void setAddress(String address) {
    _address = address;
    //保存
    SharedPreferencesUtil.saveData<String>("address", address);
    notifyListeners();
  }

  String get readInfoErr => _readInfoErr;
  void setReadInfoErr(String readInfoErr) {
    _readInfoErr = readInfoErr;
    //保存
    SharedPreferencesUtil.saveData<String>("readInfoErr", readInfoErr);
    notifyListeners();
  }

  String get currentOperation => _currentOperation;
  void setCurrentOperation(String currentOperation) {
    _currentOperation = currentOperation;
    //保存
    SharedPreferencesUtil.saveData<String>("currentOperation", currentOperation);
    notifyListeners();
  }

  String get confirmVin => _confirmVin;
  void setconfirmVin(String confirmVin) {
    _confirmVin = confirmVin;
    //保存
    SharedPreferencesUtil.saveData<String>("confirmVin", confirmVin);
    notifyListeners();
  }

  String get operationType => _operationType;
  void setOperationType(String operationType) {
    _operationType = operationType;
    //保存
    SharedPreferencesUtil.saveData<String>("operationType", operationType);
    notifyListeners();
  }

  String get carSystem => _carSystem;
  void setCarSystem(String carSystem) {
    _carSystem = carSystem;
    //保存
    SharedPreferencesUtil.saveData<String>("carSystem", carSystem);
    notifyListeners();
  }

  String get vag => _vag;
  void setVag(String vag) {
    _vag = vag;
    //保存
    SharedPreferencesUtil.saveData<String>("vag", vag);
    notifyListeners();
  }

  String get systemVersion => _systemVersion;
  void setSystemVersion(String systemVersion) {
    _systemVersion = systemVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("systemVersion", systemVersion);
    notifyListeners();
  }

  String get vin => _vin;
  void setVin(String vin) {
    _vin = vin;
    //保存
    SharedPreferencesUtil.saveData<String>("vin", vin);
    notifyListeners();
  }

  String get keyCount => _keyCount;
  void setKeyCount(String keyCount) {
    _keyCount = keyCount;
    //保存
    SharedPreferencesUtil.saveData<String>("keyCount", keyCount);
    notifyListeners();
  }

  String get currentKeyId => _currentKeyId;
  void setCurrentKeyId(String currentKeyId) {
    _currentKeyId = currentKeyId;
    //保存
    SharedPreferencesUtil.saveData<String>("currentKeyId", currentKeyId);
    notifyListeners();
  }

  String get currentReadKeyId => _currentReadKeyId;
  void setCurrentReadKeyId(String currentReadKeyId) {
    _currentReadKeyId = currentReadKeyId;
    //保存
    SharedPreferencesUtil.saveData<String>("currentReadKeyId", _currentReadKeyId);
    notifyListeners();
  }

  String get waitWriteKeyId => _waitWriteKeyId;
  void setWaitWriteKeyId(String waitWriteKeyId) {
    _waitWriteKeyId = waitWriteKeyId;
    //保存
    SharedPreferencesUtil.saveData<String>("waitWriteKeyId", _waitWriteKeyId);
    notifyListeners();
  }

  String get loginName => _loginName;
  void setLoginName(String loginName) {
    _loginName = loginName;
    //保存
    SharedPreferencesUtil.saveData<String>("loginName", _loginName);
    notifyListeners();
  }


  int get readCarDataCount => _readCarDataCount;
  void setReadCarDataCount(int readCarDataCount) {
    _readCarDataCount = readCarDataCount;
    //保存
    SharedPreferencesUtil.saveData<int>("readCarDataCount", _readCarDataCount);
    notifyListeners();
  }

  int get systemId => _systemId;
  void setSystemId(int systemId) {
    _systemId = systemId;
    //保存
    SharedPreferencesUtil.saveData<int>("systemId", _systemId);
    notifyListeners();
  }

  int get comfortableCarTypeId => _comfortableCarTypeId;
  void setComfortableCarTypeId(int comfortableCarTypeId) {
    _comfortableCarTypeId = comfortableCarTypeId;
    //保存
    SharedPreferencesUtil.saveData<int>("comfortableCarTypeId", _comfortableCarTypeId);
    notifyListeners();
  }

  int get matchTypeId => _matchTypeId;
  void setMatchTypeId(int matchTypeId) {
    _matchTypeId = matchTypeId;
    //保存
    SharedPreferencesUtil.saveData<int>("matchTypeId", _matchTypeId);
    notifyListeners();
  }

  int get operationId => _operationId;
  void setOperationId(int operationId) {
    _operationId = operationId;
    //保存
    SharedPreferencesUtil.saveData<int>("operationId", _operationId);
    notifyListeners();
  }

  int get currentUpdateIndex => _currentUpdateIndex;
  void setCurrentUpdateIndex(int currentUpdateIndex) {
    _currentUpdateIndex = currentUpdateIndex;
    //保存
    SharedPreferencesUtil.saveData<int>("currentUpdateIndex", _currentUpdateIndex);
    notifyListeners();
  }

  int get requestFailedCount => _requestFailedCount;
  void setRequestFailedCount(int requestFailedCount) {
    _requestFailedCount = requestFailedCount;
    //保存
    SharedPreferencesUtil.saveData<int>("requestFailedCount", _requestFailedCount);
    notifyListeners();
  }

  int get currentKeyIndex => _currentKeyIndex;
  void setCurrentKeyIndex(int currentKeyIndex) {
    _currentKeyIndex = currentKeyIndex;
    //保存
    SharedPreferencesUtil.saveData<int>("currentKeyIndex", currentKeyIndex);
    notifyListeners();
  }

  int get currentSelectedKeyIndex => _currentSelectedKeyIndex;
  void setCurrentSelectedKeyIndex(int currentSelectedKeyIndex) {
    _currentSelectedKeyIndex = currentSelectedKeyIndex;
    //保存
    SharedPreferencesUtil.saveData<int>("currentSelectedKeyIndex", currentSelectedKeyIndex);
    notifyListeners();
  }

  String get keyType => _keyType;
  void setKeyType(String keyType) {
    _keyType = keyType;
    //保存
    SharedPreferencesUtil.saveData<String>("keyType", keyType);
    notifyListeners();
  }

  String get writeKeyId => _writeKeyId;
  void setWriteKeyId(String writeKeyId) {
    _writeKeyId = writeKeyId;
    //保存
    SharedPreferencesUtil.saveData<String>("writeKeyId", writeKeyId);
    notifyListeners();
  }

  String get readKeySn => _readKeySn;
  void setReadKeySn(String readKeySn) {
    _readKeySn = readKeySn;
    //保存
    SharedPreferencesUtil.saveData<String>("readKeySn", _readKeySn);
    notifyListeners();
  }

  String get readKeyDistinguish => _readKeyDistinguish;
  void setReadKeyDistinguish(String readKeyDistinguish) {
    _readKeyDistinguish = readKeyDistinguish;
    //保存
    SharedPreferencesUtil.saveData<String>("readKeyDistinguish", _readKeyDistinguish);
    notifyListeners();
  }

  String get readKeyInfo => _readKeyInfo;
  void setReadKeyInfo(String readKeyInfo) {
    _readKeyInfo = readKeyInfo;
    //保存
    SharedPreferencesUtil.saveData<String>("readKeyInfo", _readKeyInfo);
    notifyListeners();
  }

  String get authFailInfo => _authFailInfo;
  void setAuthFailInfo(String authFailInfo) {
    _authFailInfo = authFailInfo;
    //保存
    SharedPreferencesUtil.saveData<String>("authFailInfo", _authFailInfo);
    notifyListeners();
  }

  String get androidDeviceId => _androidDeviceId;
  void setAndroidDeviceId(String androidDeviceId) {
    _androidDeviceId = androidDeviceId;
    //保存
    SharedPreferencesUtil.saveData<String>("androidDeviceId", _androidDeviceId);
    notifyListeners();
  }

  String get macString => _macString;
  void setMacString(String macString) {
    _macString = macString;
    //保存
    SharedPreferencesUtil.saveData<String>("macString", _macString);
    notifyListeners();
  }

  String get signString => _signString;
  void setSignString(String signString) {
    _signString = signString;
    //保存
    SharedPreferencesUtil.saveData<String>("signString", _signString);
    notifyListeners();
  }

  String get hasDialogShow => _hasDialogShow;
  void setHasDialogShow(String hasDialogShow) {
    _hasDialogShow = hasDialogShow;
    //保存
    SharedPreferencesUtil.saveData<String>("hasDialogShow", _hasDialogShow);
    notifyListeners();
  }

  String get readKeyId => _readKeyId;
  void setReadKeyId(String readKeyId) {
    _readKeyId = readKeyId;
    //保存
    SharedPreferencesUtil.saveData<String>("readKeyId", readKeyId);
    notifyListeners();
  }

  String get fileString => _fileString;
  void setFileString(String fileString) {
    _fileString = fileString;
    //保存
    SharedPreferencesUtil.saveData<String>("fileString", _fileString);
    notifyListeners();
  }

  String get carKey1 => _carKey1;
  void setCarKey1(String carKey1) {
    _carKey1 = carKey1;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey1", carKey1);
    notifyListeners();
  }

  String get carKey2 => _carKey2;
  void setCarKey2(String carKey2) {
    _carKey2 = carKey2;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey2", carKey2);
    notifyListeners();
  }

  String get carKey3 => _carKey3;
  void setCarKey3(String carKey3) {
    _carKey3 = carKey3;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey3", carKey3);
    notifyListeners();
  }

  String get carKey4 => _carKey4;
  void setCarKey4(String carKey4) {
    _carKey4 = carKey4;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey4", carKey4);
    notifyListeners();
  }

  String get carKey5 => _carKey5;
  void setCarKey5(String carKey5) {
    _carKey5 = carKey5;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey5", carKey5);
    notifyListeners();
  }

  String get carKey6 => _carKey6;
  void setCarKey6(String carKey6) {
    _carKey6 = carKey6;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey6", carKey6);
    notifyListeners();
  }

  String get carKey7 => _carKey7;
  void setCarKey7(String carKey7) {
    _carKey7 = carKey7;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey7", carKey7);
    notifyListeners();
  }

  String get carKey8 => _carKey8;
  void setCarKey8(String carKey8) {
    _carKey8 = carKey8;
    //保存
    SharedPreferencesUtil.saveData<String>("carKey8", carKey8);
    notifyListeners();
  }

  List get originKeyList => _originKeyList;
  void setOriginKeyList(List originKeyList) {
    _originKeyList = originKeyList;
    //保存
    SharedPreferencesUtil.saveData<List>("originKeyList", originKeyList);
    notifyListeners();
  }

  List get key3List => _key3List;
  void setKey3List(List key3List) {
    _key3List = key3List;
    //保存
    SharedPreferencesUtil.saveData<List>("key3List", key3List);
    notifyListeners();
  }

  List get lastKeyList => _lastKeyList;
  void setLastKeyList(List lastKeyList) {
    _lastKeyList = lastKeyList;
    //保存
    SharedPreferencesUtil.saveData<List>("lastKeyList", lastKeyList);
    notifyListeners();
  }

  List get baseKeyIdList => _baseKeyIdList;
  void setBaseKeyIdList(List baseKeyIdList) {
    _baseKeyIdList = baseKeyIdList;
    //保存
    SharedPreferencesUtil.saveData<List>("baseKeyIdList", baseKeyIdList);
    notifyListeners();
  }

  List get writeKeyIdList => _writeKeyIdList;
  void setWriteKeyIdList(List writeKeyIdList) {
    _writeKeyIdList = writeKeyIdList;
    //保存
    SharedPreferencesUtil.saveData<List>("writeKeyIdList", writeKeyIdList);
    notifyListeners();
  }

  List get mqbWriteKeyIdList => _mqbWriteKeyIdList;
  void setMqbWriteKeyIdList(List mqbWriteKeyIdList) {
    _mqbWriteKeyIdList = mqbWriteKeyIdList;
    //保存
    SharedPreferencesUtil.saveData<List>("mqbWriteKeyIdList", _mqbWriteKeyIdList);
    notifyListeners();
  }

  List get writeKeyDataList => _writeKeyDataList;
  void setWriteKeyDataList(List writeKeyDataList) {
    _writeKeyDataList = writeKeyDataList;
    //保存
    SharedPreferencesUtil.saveData<List>("writeKeyDataList", _writeKeyDataList);
    notifyListeners();
  }

  String get mainChipBootVersion => _mainChipBootVersion;
  void setMainChipBootVersion(String mainChipBootVersion) {
    _mainChipBootVersion = mainChipBootVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("mainChipBootVersion", _mainChipBootVersion);
    notifyListeners();
  }

  String get mainChipAppVersion => _mainChipAppVersion;
  void setMainChipAppVersion(String mainChipAppVersion) {
    _mainChipAppVersion = mainChipAppVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("mainChipAppVersion", _mainChipAppVersion);
    notifyListeners();
  }

  String get chip1BootVersion => _chip1BootVersion;
  void setChip1BootVersion(String chip1BootVersion) {
    _chip1BootVersion = chip1BootVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("chip1BootVersion", _chip1BootVersion);
    notifyListeners();
  }

  String get chip1AppVersion => _chip1AppVersion;
  void setChip1AppVersion(String chip1AppVersion) {
    _chip1AppVersion = chip1AppVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("chip1AppVersion", _chip1AppVersion);
    notifyListeners();
  }

  String get chip2BootVersion => _chip2BootVersion;
  void setChip2BootVersion(String chip2BootVersion) {
    _chip2BootVersion = chip2BootVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("chip2BootList", _chip2BootVersion);
    notifyListeners();
  }

  String get chip2AppVersion => _chip2AppVersion;
  void setChip2AppVersion(String chip2AppVersion) {
    _chip2AppVersion = chip2AppVersion;
    //保存
    SharedPreferencesUtil.saveData<String>("chip2AppVersion", _chip2AppVersion);
    notifyListeners();
  }

  List get fileDataList => _fileDataList;
  void setFileDataList(List fileDataList) {
    _fileDataList = fileDataList;
    //保存
    SharedPreferencesUtil.saveData<List>("fileDataList", _fileDataList);
    notifyListeners();
  }

  List get originFileDataList => _originFileDataList;
  void setOriginFileDataList(List originFileDataList) {
    _originFileDataList = originFileDataList;
    //保存
    SharedPreferencesUtil.saveData<List>("originFileDataList", _originFileDataList);
    notifyListeners();
  }

  StreamSubscription<Map<String, Object>> get locationListener => _locationListener;
  void setLocationListener(StreamSubscription<Map<String, Object>> locationListener) {
    _locationListener = locationListener;
    //保存
    SharedPreferencesUtil.saveData<StreamSubscription<Map<String, Object>>>("locationListener", locationListener);
    notifyListeners();
  }

  List get readKeyData => _readKeyData;
  void setReadKeyData(List readKeyData) {
    _readKeyData = readKeyData;
    //保存
    SharedPreferencesUtil.saveData<List>("readKeyData", readKeyData);
    notifyListeners();
  }

  List get learnKeyList => _learnKeyList;
  void setLearnKeyList(List learnKeyList) {
    _learnKeyList = learnKeyList;
    //保存
    SharedPreferencesUtil.saveData<List>("learnKeyList", learnKeyList);
    notifyListeners();
  }

  bool get isUpdateSuccess => _isUpdateSuccess;
  void setIsUpdateSuccess(bool isUpdateSuccess) {
    _isUpdateSuccess = isUpdateSuccess;
    SharedPreferencesUtil.saveData<bool>("isUpdateSuccess", _isUpdateSuccess);
    notifyListeners();
  }

  bool get getChipVersion => _getChipVersion;
  void setGetChipVersion(bool getChipVersion) {
    _getChipVersion = getChipVersion;
    SharedPreferencesUtil.saveData<bool>("getChipVersion", _getChipVersion);
    notifyListeners();
  }

  bool get openEngine => _openEngine;
  void setOpenEngine(bool openEngine) {
    _openEngine = openEngine;
    SharedPreferencesUtil.saveData<bool>("openEngine", openEngine);
    notifyListeners();
  }

  bool get isStartUpdate => _isStartUpdate;
  void setIsStartUpdate(bool isStartUpdate) {
    _isStartUpdate = isStartUpdate;
    SharedPreferencesUtil.saveData<bool>("isStartUpdate", _isStartUpdate);
    notifyListeners();
  }

  bool get isLastPackage => _isLastPackage;
  void setIsLastPackage(bool isLastPackage) {
    _isLastPackage = isLastPackage;
    SharedPreferencesUtil.saveData<bool>("isLastPackage", _isLastPackage);
    notifyListeners();
  }

  bool get openIgnition => _openIgnition;
  void setOpenIgnition(bool openIgnition) {
    _openIgnition = openIgnition;
    SharedPreferencesUtil.saveData<bool>("openIgnition", openIgnition);
    notifyListeners();
  }

  bool get uploadFailed => _uploadFailed;
  void setUploadFailed(bool uploadFailed) {
    _uploadFailed = uploadFailed;
    SharedPreferencesUtil.saveData<bool>("uploadFailed", uploadFailed);
    notifyListeners();
  }

  bool get isOpenGps => _isOpenGps;
  void setIsOpenGps(bool isOpenGps) {
    _isOpenGps = isOpenGps;
    SharedPreferencesUtil.saveData<bool>("openEngine", _isOpenGps);
    notifyListeners();
  }

  String get learnKeyCount => _learnKeyCount;
  void setLearnKeyCount(String learnKeyCount) {
    _learnKeyCount = learnKeyCount;
    //保存
    SharedPreferencesUtil.saveData<String>("learnKeyCount", learnKeyCount);
    notifyListeners();
  }

  BluetoothDevice get bluetoothDevice => _bluetoothDevice;
  void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
    _bluetoothDevice = bluetoothDevice;
    //保存
    SharedPreferencesUtil.saveData<BluetoothDevice>("bluetoothDevice", bluetoothDevice);
    notifyListeners();
  }

  String get clientmobile => _clientmobile;
  void setClientmobile(String clientmobile) {
    _clientmobile = clientmobile;
    //保存
    SharedPreferencesUtil.saveData<String>("mobile", clientmobile);
    notifyListeners();
  }

  // String get ownerphone => _ownerphone;
  // void setOwnerphone(String ownerphone) {
  //   _ownerphone = ownerphone;
  //   notifyListeners();
  // }

  String get uptime => _uptime;
  void setUptime(String uptime) {
    _uptime = uptime;
    notifyListeners();
  }

  String get mileage => _mileage;
  void setMileage(String mileage) {
    _mileage = mileage;
    notifyListeners();
  }

  String get carno => _carno;
  void setCarno(String carno) {
    _carno = carno;
    SharedPreferencesUtil.saveData<String>("carno", carno);
    notifyListeners();
  }

  String get devname => _devname;
  void setDevname(String devname) {
    _devname = devname;
    //保存
    SharedPreferencesUtil.saveData<String>("devname", devname);
    notifyListeners();
  }

  String get brandAndModel => _brandAndModel;
  void setBrandAndModel(String brandAndModel) {
    _brandAndModel = brandAndModel;
    notifyListeners();
  }

  String get oil => _oil;
  void setOil(String oil) {
    _oil = oil;
    notifyListeners();
  }

  String get bat => _bat;
  void setBat(String bat) {
    _bat = bat;
    notifyListeners();
  }

  String get careditcarid => _careditcarid;
  void setCareditcarid(String careditcarid) {
    _careditcarid = careditcarid;
    notifyListeners();
  }

  String get careditdevid => _careditdevid;
  void setCareditdevid(String careditdevid) {
    _careditdevid = careditdevid;
    notifyListeners();
  }

  String get lendid => _lendid;
  void setLendid(String lendid) {
    _lendid = lendid;
    notifyListeners();
  }

  double get temp => _temp;
  void setTemp(double temp) {
    _temp = temp;
    notifyListeners();
  }

  bool get borrowstart => _borrowstart;
  void setBorrowstart(bool borrowstart) {
    _borrowstart = borrowstart;
    notifyListeners();
  }

  bool get borrowlock => _borrowlock;
  void setBorrowlock(bool borrowlock) {
    _borrowlock = borrowlock;
    notifyListeners();
  }

  bool get borrowlocation => _borrowlocation;
  void setBorrowlocation(bool borrowlocation) {
    _borrowlocation = borrowlocation;
    notifyListeners();
  }

  bool get borrowtrace => _borrowtrace;
  void setBorrowtrace(bool borrowtrace) {
    _borrowtrace = borrowtrace;
    notifyListeners();
  }


  ///大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
  /// 此方法中前三位格式有：
  /// 13+任意数 * 15+除4的任意数 * 18+除1和4的任意数 * 17+除9的任意数 * 147
  bool isChinaPhoneLegal(String str) {
    return new RegExp('^((13[0-9])|(15[^4])|(166)|(17[0-8])|(18[0-9])|(19[8-9])|(147,145))\\d{8}\$').hasMatch(str);
  }

  bool isEmail(String str) {
    return RegExp(
        r"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")
        .hasMatch(str);
  }


  void exitLogin(String msgText){
    BluetoothConnection bluetoothConnection = BluetoothConnection.blueToothSingleton();
    if(bluetoothConnection.bluetoothDevice!=null){
      bluetoothConnection.disconnect();
    }
    // if(_isconnect == 1){
    //   bluetoothConnection.disconnect();
    // }

    //SharedPreferencesUtil.saveData<String>("token", null);
    SpUtil.putString(Constant.accessToken, '');
    cleanAll();
    ShowDialog().alertDialog(msgText);
  }

  List   _logList = List();

  List get logList => _logList;
  void setLogList(List logList) {
    _logList = logList;
    //保存
    SharedPreferencesUtil.saveData<List>("logList", _logList);
    notifyListeners();
  }

}
