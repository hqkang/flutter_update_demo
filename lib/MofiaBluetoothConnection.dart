import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_blue/flutter_blue.dart' as flutter_blue; //
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart' as flutter_bluetooth;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_demo/CtrApp.dart';
import 'package:flutter_demo/RouterKey.dart';
import 'package:flutter_demo/ShowDialog.dart';
import 'package:flutter_demo/convertUtil.dart';
import 'package:flutter_demo/sp_util.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:wifi/wifi.dart';

///(奥多)蓝牙连接设备验证
class MofiaBluetoothConnection {
  static MofiaBluetoothConnection _instance;
  flutter_bluetooth.BluetoothConnection connection;
  bool isDisconnecting = false;
  BuildContext context;
  flutter_blue.FlutterBlue  flutterBlue = flutter_blue.FlutterBlue.instance;
  ArsProgressDialog progressDialog; //连接弹框
  ArsProgressDialog waitDialog; // 等待弹窗
  bool isDiscovering = true;
  static const stream = MethodChannel("flutter_plugin_update");
  String deviceIp = '';
  bool blueTurnOn = false; //蓝牙功能是否打开
  Timer _retriveTimer;
  int serial_cnt = 0; //流水号

  static MofiaBluetoothConnection blueToothSingleton() {
    if (_instance == null) {
      _instance = MofiaBluetoothConnection._internal();
    }
    return _instance;
  }

  Future<InternetAddress> get selfIP async {
    String ip = await Wifi.ip;
    deviceIp = ip;
    print('设备IP: ' + deviceIp + '${InternetAddress(ip)}');
    return InternetAddress(ip);
  }

  Future<String> installBegin() async {
    Uint8List success;
    String message;
    try {
      final result = await stream.invokeMethod("installBegin");
      success = result;

      sendMessage(success);
      print('message-----: ${success}');
    } on PlatformException {
      message = "errormessage";
    }


    // print("write(HEX.decode(message)):${HEX.decode(message)}");
  }

  Future<String> initSocketUtil() async {
    await selfIP;
    print('传递的ip: ' + deviceIp);
    Map data = {"ip" : deviceIp};
    Uint8List success;
    String message;
    try {
      final result = await stream.invokeMethod("initSocketUtil", data);
      print("传过去的数据=========map:$data");
      success = result;

      // sendMessage(success);
      print('message-----: ${success}');
    } on PlatformException {
      message = "errormessage";
    }


    // print("write(HEX.decode(message)):${HEX.decode(message)}");
  }

  Future<String> retriveData() async {

    Uint8List success;
    String message;
    try {
      final result = await stream.invokeMethod("retriveData");
      if (result.length > 0){
        success = ConvertUtil().stringToList(result);
        sendMessage(success);
        // sendData('058000000000');
      }

      // message = result;
      // sendMessage(success);

      // 058000000000
      print('retriveMsg-----: ${success}');
    } on PlatformException {
      message = "errormessage";
    }


    // print("write(HEX.decode(message)):${HEX.decode(message)}");
  }

  Future<String> packageSend() async {

      Map data = {"data" : RouterKey.navigatorKey.currentContext.read<CtrApp>().fileString};
      Uint8List success;
      String message;
      try {
        final result = await stream.invokeMethod("startUpdate", data);
        print("传过去的数据=========map:$data");
        success = ConvertUtil().stringToList(result);

        // sendMessage(success);
        sendData('058000000000');

        // 058000000000
        print('message-----: ${success} ---- messageStr---: ${result}');
      } on PlatformException {
        message = "errormessage";
      }


    // print("write(HEX.decode(message)):${HEX.decode(message)}");
  }

  Future<String> sendData(String result) async {

    Map data = {"bluetoothData" : result};
    Uint8List success;
    String message;
    try {
      final result = await stream.invokeMethod("handleBluetoothData", data);
      print("传过去的数据=========map:$data");
      success = ConvertUtil().stringToList(result);

      sendMessage(success);
      print('message-----: ${success}');
    } on PlatformException {
      message = "errormessage";
    }


    // print("write(HEX.decode(message)):${HEX.decode(message)}");
  }

  MofiaBluetoothConnection._internal() {
    flutterBlue = flutter_blue.FlutterBlue.instance;
    print("初始化啦啊");
    _retriveTimer = Timer.periodic(Duration(milliseconds: 500), (t) {
      retriveData();
    });
  }

  ///查看蓝牙是否开启
  void checkBluetoothTurningOn(BuildContext context) {
    this.context = context;
    this.context = RouterKey.navigatorKey.currentContext;
    print('当前的context: ${context}');
    //监听蓝牙状态
    flutterBlue.state.listen((value) async {
      print("监听蓝牙状态: $value");
      if (value == flutter_blue.BluetoothState.off) {
        context.read<CtrApp>().setIsconnect(0);
        context.read<CtrApp>().setIsOpenBlue(0);
        if (blueTurnOn == false) {
        }
        blueTurnOn = false;
        return;
      } else if (value == flutter_blue.BluetoothState.on) {
        context.read<CtrApp>().setIsOpenBlue(1);
        blueTurnOn = true;
      }
    });
  }

  ///开始连接提示弹框
  void showConnectionStart(BuildContext context) {
    progressDialog = ArsProgressDialog(context,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 100.0,
          width: 300.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Row(
            children: [
              CircularProgressIndicator(
                strokeWidth: 2,
              ),
              SizedBox(width: 20),
              Text("正在连接蓝牙 ...",
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      decoration: TextDecoration.none),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    progressDialog.show(); // show dialog
  }

  ///等待提示弹框
  Future showWaitDialogStart(String tip) {
    waitDialog = ArsProgressDialog(RouterKey.navigatorKey.currentContext,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 180.0,
          width: 200.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Column(
            children: [
              SizedBox(height: 24),
              CircularProgressIndicator(
                strokeWidth: 2,
              ),
              SizedBox(height: 24),
              Text(
                  tip,
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      decoration: TextDecoration.none),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    waitDialog.show(); // show dialog
  }

  //蓝牙连接结果显示
  void showConnectionResult(String content) {
    ShowDialog().alertDialog(content);
  }

  void showBleConnectSuccess(){
    showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: RouterKey.navigatorKey.currentContext,
        builder: (context) {
          return Center(
              child: Container(
                  width: 280,
                  height: 180,
                  padding: EdgeInsets.fromLTRB(5, 20, 5, 5),
                  decoration:BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))
                  ),
                  child: Column(
                    children: [
                      Expanded(
                          flex:1,
                          child: Center(
                              child: Text('蓝牙连接成功', style: TextStyle(fontSize: 14, color: Colors.black, decoration: TextDecoration.none), textAlign: TextAlign.center)
                          )
                      ),
                      Divider(),
                      SizedBox(
                        width: 280,
                        height: 45,
                        child: FlatButton(
                          child: Text("关闭"),
                          onPressed: (){
                            RouterKey.navigatorKey.currentState.pop();
                            RouterKey.navigatorKey.currentState.pop();
                          },
                        ),
                      )
                    ],
                  )
              )
          );
        }
    );
  }

  // 蓝牙回调
  Future _onDataReceived(Uint8List packet) async{
    await SpUtil.getInstance();
    String s = ConvertUtil().listTo16String(packet);
    print('data: ${packet}-------s: ${s}');
    sendData(s);
    // Fluttertoast.showToast(msg: s);
    List tempList = context.read<CtrApp>().logList;
    if (tempList.length == 0){
      tempList = List();
    }
    List logList = List.from(tempList);

    if (s.contains('058F')){
      logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '升级失败' + s + ConvertUtil().intTo16String(serial_cnt));
    }else{
      logList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '收到的数据' + s + ConvertUtil().intTo16String(serial_cnt));
    }
    context.read<CtrApp>().setLogList(logList);
    serial_cnt++;
    if (s.length > 4) {
      if (packet[0]==0x01){

      }else if (packet[0] ==  0x40) {
        // System.arraycopy(allbyte, 0, data, 0, size);
        //System.out.println("0x81========="+byte2HexStr(allbyte));
        // ispost = 0;
        if (packet[3] == 0x80) {
          //说明是信息
          if (packet[4] == 0x00) {
            if (packet[5] == 0x55 && packet[6] == 0x5a){
              if (packet[7] == 0x00 && packet[8] == 0x00){

              } else if (packet[7] == 0x00 && packet[8] == 0x01){
                print('读取信息成功');
              }else if (packet[7] == 0x00 && packet[8] == 0x00){
                // print('等待');
              } else if (packet[7] == 0x00 && packet[8] == 0x02){

              }else if (packet[7] == 0x00 && packet[8] == 0x03){// 自动识别完成

              }else if (packet[7] == 0x10 && packet[8] == 0x00){// 未将钥匙靠近车辆感应区

              }else if (packet[7] == 0x20 && packet[8] == 0x01){// 第一把遥控器匹配成功

              }else if (packet[7] == 0x20 && packet[8] == 0x02){// 第二把遥控器匹配成功

              }else if (packet[7] == 0x20 && packet[8] == 0x03){// 第三把遥控器匹配成功

              }else if (packet[7] == 0x20 && packet[8] == 0x04) { // 第四把遥控器匹配成功
              }
            }

          } else if (packet[4] == 0x23){
            RouterKey.navigatorKey.currentState.pop();
            // 芯片1
            List chip1 = List();
            String chipStr = '';
            if (packet.length >= 17){
              for (var i = 5; i < 17; i++){
                chip1.add(packet[i]);
              }
            }
            chipStr = ConvertUtil().listTo16String(chip1);
            await SpUtil.putString('chip1', chipStr);
            List tempList = RouterKey.navigatorKey.currentContext.read<CtrApp>().logList;
            if (tempList.length == 0){
              tempList = List();
            }
            tempList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '芯片1--${chipStr}');
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setLogList(tempList);
            print('芯片1: ${chipStr}');
            // 芯片2
            List chip2 = List();
            String chipStr2 = '';
            if (packet.length >= 33){
              for (var i = 17; i < 33; i++){
                chip2.add(packet[i]);
              }
            }
            chipStr2 = ConvertUtil().listTo16String(chip2);
            await SpUtil.putString('chip2', chipStr2);
            List tempList2 = RouterKey.navigatorKey.currentContext.read<CtrApp>().logList;
            if (tempList2.length == 0){
              tempList2 = List();
            }
            tempList2.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '芯片2--${chipStr2}');
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setLogList(tempList2);
            print('芯片2: ${chipStr2}');
            // 蓝牙广播数据
            List bleDataList = List();
            String bleDataStr = '';
            if (packet.length >= 45){
              for (var i = 35; i < 45; i++){
                bleDataList.add(packet[i]);
              }
            }
            bleDataStr = ConvertUtil().listTo16String(bleDataList);
            bool isSuccess = await SpUtil.putString('bleData', bleDataStr);
            List tempList3 = RouterKey.navigatorKey.currentContext.read<CtrApp>().logList;
            if (tempList3.length == 0){
              tempList3 = List();
            }
            tempList3.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + '广播数据--${bleDataStr}');
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setLogList(tempList3);
            print('广播数据: ${bleDataStr}-----bledata: ${SpUtil.getString('bleData')}---isSuccess: ${isSuccess}-----${RouterKey.navigatorKey.currentContext.read<CtrApp>().logList}');
            if (chipStr.length > 0 && chipStr2.length > 0 && bleDataStr.length > 0){
              showConnectionResult('获取成功');
            }else{
              RouterKey.navigatorKey.currentState.pop();
              Fluttertoast.showToast(msg: '数据有问题');
            }
          } else if (packet[4] == 0x01) {

          } else if (packet[4] == 0x02) {

          } else if (packet[4] == 0x03) {
            //获取匹配状态
            List<int> vvin = List();
            if (packet[5] == 0x00) {
              // app.blemsg = "无车辆信息,请打开ON后再重新获取匹配状态";
              // print('无车辆信息,请打开ON后再重新获取匹配状态');
            }
          } else if (packet[4] == 0x0a) {

          }
        } else if (packet[3] == 0x81) {

        }else if(packet[3] == 0x90 && packet[4] == 0x04){

        }else if(packet[3] == 0x90 && packet[4] == 0x06){

        }else if(packet[3] == 0x90 && packet[4] == 0x07){

        }else if(packet[3] == 0x90 && packet[4] == 0x09){ // 读取遥控器信息成功

        }else if(packet[3] == 0xFF && (packet[4] == 0x01 || packet[4] == 0x02)){
        }
      }
    }else{

    }
    try {
      print(utf8.decode(packet));
    } catch (e) {
    }
  }

  void sendMessage(List<int> list) async {
    // connection.output.add(utf8.encode("data here" + "\r\n"));
    Uint8List bytes = Uint8List.fromList(list);
    connection.output.add(bytes);
    print('byte: ${bytes}----output: ${connection.output}------${ConvertUtil().listTo16String(list)}');
    await connection.output.allSent;
  }

  // spp蓝牙连接
  Future blueConnection(String address) async{
    await SpUtil.getInstance();
    // result.device.address
    flutter_bluetooth.BluetoothConnection.toAddress(address).then((_connection) {
      print('Connected to the device-----connection:${_connection}');
      connection = _connection;
      isDisconnecting = false;
      RouterKey.navigatorKey.currentState.pop();
      context.read<CtrApp>().setIsconnect(1);
      SpUtil.putString('isConnect', '1');
      showBleConnectSuccess();
      List tempList = RouterKey.navigatorKey.currentContext.read<CtrApp>().logList;
      if (tempList.length == 0){
        tempList = List();
      }
      tempList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + address + '蓝牙连接成功');
      RouterKey.navigatorKey.currentContext.read<CtrApp>().setLogList(tempList);
      connection.input.listen(_onDataReceived).onDone(() {
        if (isDisconnecting) {
          print('Disconnecting locally!');
        } else {
          print('Disconnected remotely!');
          SpUtil.putString('isConnect', '0');
          context.read<CtrApp>().setIsconnect(0);
        }
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      RouterKey.navigatorKey.currentState.pop();
      showConnectionResult("蓝牙连接失败");
      SpUtil.putString('isConnect', '0');
      context.read<CtrApp>().setIsconnect(0);
      List tempList = RouterKey.navigatorKey.currentContext.read<CtrApp>().logList;
      if (tempList.length == 0){
        tempList = List();
      }
      tempList.add(formatDate(DateTime.now(), [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss]) + ': ' + address + '蓝牙连接失败');
      RouterKey.navigatorKey.currentContext.read<CtrApp>().setLogList(tempList);
      print(error);
    });
  }

  Future disConnection(String address) async{
    await flutter_bluetooth.FlutterBluetoothSerial.instance
        .removeDeviceBondWithAddress(address);
  }


  stopScan() {
    print("停止啦");
  }
}

