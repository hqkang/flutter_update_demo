import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:convert_hex/convert_hex.dart'; //
import 'package:flustars/flustars.dart'; //
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_blue/flutter_blue.dart' as flutter_blue; //
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart' as flutter_bluetooth;
import 'package:flutter_demo/CtrApp.dart';
import 'package:flutter_demo/RouterKey.dart';
import 'package:flutter_demo/ShowDialog.dart';
import 'package:flutter_demo/convertUtil.dart';
import 'package:flutter_demo/custom_dialog.dart';
import 'package:flutter_demo/dio_utils.dart';
import 'package:flutter_demo/http_api.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_demo/common.dart';
import 'package:provider/provider.dart';

///(奥多)蓝牙连接设备验证
class BluetoothConnection {
  static BluetoothConnection _instance;
  StreamSubscription<flutter_bluetooth.BluetoothDiscoveryResult> streamSubscription;
  List<flutter_bluetooth.BluetoothDiscoveryResult> results = List<flutter_bluetooth.BluetoothDiscoveryResult>();
  List addressList = List();
  bool isDiscovering = true;
  flutter_bluetooth.BluetoothConnection connection;
  bool isDisconnecting = false;
  BuildContext context;
  flutter_blue.FlutterBlue  flutterBlue = flutter_blue.FlutterBlue.instance;
  flutter_blue.BluetoothDevice bluetoothDevice;
  flutter_bluetooth.BluetoothDevice bluetoothDevice2;
  ArsProgressDialog progressDialog; //连接弹框
  ArsProgressDialog writeDialog; //写长数据弹框
  ArsProgressDialog autoDialog; // 自动识别指令-钥匙未靠近感应区弹窗
  ArsProgressDialog waitDialog; // 等待弹窗
  ArsProgressDialog waitDialog2; // 等待弹窗
  ArsProgressDialog waitDialog3; // 等待弹窗
  ArsProgressDialog readDataSuccessDialog; // 读取信息成功弹窗
  ArsProgressDialog learnKeyDialog; // 学习钥匙弹窗
  int file_cnt = 0; //流水号
  int learnKeyCount = 0;
  int currentKeyIndex = 0;
  double currentProgress = 0.0;
  String currentProgressStr = "当前进度: ";
  String bleType = '1'; // 0 代表ble  1 代表spp
  int requestCount = 0;
  List keyList = List();
  List key3List = List();
  int car_status;
  Timer _timer, _outTimer, _requestTimer, _requestOutTimer;
  List<flutter_blue.ScanResult> scanResultList = List<flutter_blue.ScanResult>();

  int size = 0;
  List bleByteData = List();
  String confirmVin = '';
  String pid = '';
  String bledata = '';
  String carstyle = '';

  bool blueDevChange = false;

  bool blueConnecting = false;

  bool blueTurnOn = false; //蓝牙功能是否打开

  bool isShow = true; //未搜索到设备显示一次弹框

  bool connectDev = false; //连接设备

  bool requestEmpty = false; //返回的数据

  bool identification = false; //认证

  int serial_cnt = 1; //流水号
  int secrettype = 1; //是否加密。。。 1 加密

  static const stream = MethodChannel("androidBluetooth");

  //交互的通道名称，flutter和native是通过这个标识符进行相互间的通信 IOS
  static const communicateChannel =
  MethodChannel('https://www.jianshu.com/u/ee3db73e5459');

  flutter_blue.BluetoothCharacteristic characteristicWrite;
  String deviceName = "867047041550263";

  //String deviceName = "AOD T002DC700";
  String deviceSecret = '6421374045557a406446424e23667721';
  // String deviceSecret = SpUtil.getString(Constant.btKey);

  /// Scanning
  StreamSubscription _scanSubscription;
  StreamSubscription _blueStatesubcription;
  StreamSubscription _blueCheck;

  static BluetoothConnection blueToothSingleton() {
    if (_instance == null) {
      _instance = BluetoothConnection._internal();
    }
    return _instance;
  }

  BluetoothConnection._internal() {
    flutterBlue = flutter_blue.FlutterBlue.instance;
    print("初始化啦啊");
  }

  ///查看蓝牙是否开启
  void checkBluetoothTurningOn(BuildContext context) {
    this.context = context;
    this.context = RouterKey.navigatorKey.currentContext;
    print('当前的context: ${context}');
    //监听蓝牙状态
    flutterBlue.state.listen((value) async {
      print("监听蓝牙状态: $value");
      if (value == flutter_blue.BluetoothState.off) {
        context.read<CtrApp>().setIsconnect(0);
        context.read<CtrApp>().setIsOpenBlue(0);
        if (blueTurnOn == false) {
          //isShow = false;
          // showConnectionResult("蓝牙尚未开启,请开启蓝牙后连接");
        }
        blueTurnOn = false;
        if (waitDialog != null){
          // waitDialog.dismiss();
          Future.delayed(Duration(seconds: 0), (){
            RouterKey.navigatorKey.currentState.pop();
          });
        }else{
          // print('等待弹窗2');
        }
        return;
      } else if (value == flutter_blue.BluetoothState.on) {
        context.read<CtrApp>().setIsOpenBlue(1);
        blueTurnOn = true;
      }
    });
  }

  void showBleConnectSuccess(){
    showDialog(
        barrierDismissible: false,  // 表示点击灰色背景的时候是否消失弹出框
        context: RouterKey.navigatorKey.currentContext,
        builder: (context) {
          return Center(
              child: Container(
                  width: 280,
                  height: 180,
                  padding: EdgeInsets.fromLTRB(5, 20, 5, 5),
                  decoration:BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(15.0))
                  ),
                  child: Column(
                    children: [
                      Expanded(
                          flex:1,
                          child: Center(
                              child: Text('蓝牙连接成功', style: TextStyle(fontSize: 14, color: Colors.black, decoration: TextDecoration.none), textAlign: TextAlign.center)
                          )
                      ),
                      Divider(),
                      SizedBox(
                        width: 280,
                        height: 45,
                        child: FlatButton(
                          child: Text("关闭"),
                          onPressed: (){

                          },
                        ),
                      )

                    ],
                  )
              )
          );
        }
    );
    // ArsProgressDialog dialog = ArsProgressDialog(context,
    //     blur: 0,
    //     dismissable: true,
    //     backgroundColor: Color(0x33000000),
    //     loadingWidget: Container(
    //       padding: EdgeInsets.all(20.0),
    //       height: 160,
    //       width: 200.0,
    //       alignment: Alignment.center,
    //       decoration: BoxDecoration(
    //         borderRadius: BorderRadius.circular(20),
    //         color: Colors.white,
    //       ),
    //       child: Column(
    //         children: [
    //           SizedBox(height: 16),
    //           Text('蓝牙连接成功',
    //               style: TextStyle(
    //                   fontSize: 16,
    //                   color: Colors.black,
    //                   decoration: TextDecoration.none),
    //               textAlign: TextAlign.center),
    //           SizedBox(height: 24),
    //           Container(
    //             height: 1,
    //             color: Colors.black12,
    //           ),
    //           Container(
    //             height: 50,
    //             width: 200,
    //             decoration: BoxDecoration(
    //               borderRadius: BorderRadius.circular(25),
    //               color: Colors.white,
    //             ),
    //             child: FlatButton(
    //               onPressed: (){
    //                 RouterKey.navigatorKey.currentState.pop();
    //                 NavigatorUtils.push(RouterKey.navigatorKey.currentContext, Routes.main, clearStack: true);
    //               },
    //               child: Text(
    //                 '关闭',
    //                 style: TextStyle(
    //                   fontSize: 16,
    //                   color: Colors.black,
    //                   fontWeight: FontWeight.bold
    //                 ),
    //               ),
    //             ),
    //           )
    //         ],
    //       ),
    //     ),
    //     animationDuration: Duration(milliseconds: 500));

    // dialog.show();
  }

  void _startDiscovery() {
    print('开始扫描');
    streamSubscription?.cancel();
    streamSubscription =
        flutter_bluetooth.FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
          flutter_bluetooth.BluetoothDiscoveryResult result = r;
          print('名称: ${result.device.name}--------bondState: ${result.device.bondState.stringValue}');
          // && result.device.name.contains('OBD')
          if (result.device.name != null && !addressList.contains(result.device.address)){
            results.add(r);
            addressList.add(r.device.address);
          }
        });

    streamSubscription.onDone(() {
      isDiscovering = false;
      print('扫描结束');
    });
  }

  // 连接蓝牙时请求服务器验证
  Future bleVerification(String chip1, String chip2, String sign) async {
    await DioUtils.instance.requestNetwork(
        Method.get, HttpApi.bluetoothVerification2 + '?ic1=${chip1}&ic2=${chip2}&sign=${sign}',
        onSuccess: (data){
          if (data['status'] == 200){
            print('data===${data['data']}');
            RouterKey.navigatorKey.currentState.pop();
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setHasDialogShow('1');
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setRequestId(data['msg']);
            print('devId: ${RouterKey.navigatorKey.currentContext.read<CtrApp>().requestId}');
            showBleConnectSuccess();
          }else if(data['status'] == 410000){
            RouterKey.navigatorKey.currentState.pop();
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setHasDialogShow('1');
          }else{
            RouterKey.navigatorKey.currentState.pop();
            RouterKey.navigatorKey.currentContext.read<CtrApp>().setHasDialogShow('1');
            disConnection(RouterKey.navigatorKey.currentContext.read<CtrApp>().currentBle.device.id.toString());
            showConnectionResult("蓝牙连接失败" + data['msg']);
          }
        },
        onError: (code,message){
          // Toast.show(message);
          RouterKey.navigatorKey.currentState.pop();
          RouterKey.navigatorKey.currentContext.read<CtrApp>().setHasDialogShow('1');
          disConnection(RouterKey.navigatorKey.currentContext.read<CtrApp>().currentBle.device.id.toString());
          showConnectionResult("蓝牙连接失败");
        }
    );
  }


  // 蓝牙回调方法
  void _onDataReceived(Uint8List packet) {

    String s = ConvertUtil().listTo16String(packet);
    print('data: ${packet}-------s: ${s}');

    try {
      print(utf8.decode(packet));
    } catch (e) {
    }
  }

  // 发送蓝牙指令
  void sendMessage(List<int> list) async {
    // connection.output.add(utf8.encode("data here" + "\r\n"));
    Uint8List bytes = Uint8List.fromList(list);

    connection.output.add(bytes);
    print('byte: ${bytes}----output: ${connection.output}------${ConvertUtil().listTo16String(list)}');
    await connection.output.allSent;
  }

  // spp蓝牙连接
  Future blueConnection(String address) async{
    // result.device.address
    print('蓝牙地址: ${address}');
    flutter_bluetooth.BluetoothConnection.toAddress(address).then((_connection) {
      print('Connected to the device-----connection:${_connection}');
      // RouterKey.navigatorKey.currentState.pop();
      // showBleConnectSuccess();
      connection = _connection;
      context.read<CtrApp>().setIsconnect(1);
      isDisconnecting = false;
      sendMessage([0x40, 0x00, 0x01, 0x00, 0x23]);
      connection.input.listen(_onDataReceived).onDone(() {
        // Example: Detect which side closed the connection
        // There should be `isDisconnecting` flag to show are we are (locally)
        // in middle of disconnecting process, should be set before calling
        // `dispose`, `finish` or `close`, which all causes to disconnect.
        // If we except the disconnection, `onDone` should be fired as result.
        // If we didn't except this (no flag set), it means closing by remote.

        if (isDisconnecting) {
          print('Disconnecting locally!');
          if (RouterKey.navigatorKey.currentContext.read<CtrApp>().hasDialogShow == '0'){
            RouterKey.navigatorKey.currentState.pop();
            ShowCustomTwoButtonDialog(context: RouterKey.navigatorKey.currentContext, height: 160, colors: Colors.black54, buttonTitle: '确定', title: '提示', content: '蓝牙连接断开', onTap: (){
              RouterKey.navigatorKey.currentState.pop();
            }).tipsDialog();
          }else{
            ShowCustomTwoButtonDialog(context: RouterKey.navigatorKey.currentContext, height: 160, colors: Colors.black54, buttonTitle: '确定', title: '提示', content: '蓝牙连接断开', onTap: (){
              RouterKey.navigatorKey.currentState.pop();
            }).tipsDialog();
          }
        } else {
          print('Disconnected remotely!');
          context.read<CtrApp>().setIsconnect(0);
          if (RouterKey.navigatorKey.currentContext.read<CtrApp>().hasDialogShow == '0'){
            RouterKey.navigatorKey.currentState.pop();
            ShowCustomTwoButtonDialog(context: RouterKey.navigatorKey.currentContext, height: 160, colors: Colors.black54, buttonTitle: '确定', title: '提示', content: '蓝牙连接断开', onTap: (){
              RouterKey.navigatorKey.currentState.pop();
            }).tipsDialog();
          }else{
            ShowCustomTwoButtonDialog(context: RouterKey.navigatorKey.currentContext, height: 160, colors: Colors.black54, buttonTitle: '确定', title: '提示', content: '蓝牙连接断开', onTap: (){
              RouterKey.navigatorKey.currentState.pop();
            }).tipsDialog();
          }
        }
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      RouterKey.navigatorKey.currentContext.read<CtrApp>().setIsconnect(0);
      RouterKey.navigatorKey.currentState.pop();
      showConnectionResult("蓝牙连接失败");
      print(error);
    });
  }

  Future disConnection(String address) async{
    await flutter_bluetooth.FlutterBluetoothSerial.instance
        .removeDeviceBondWithAddress(address);
  }


  void connectState() {
    _blueStatesubcription = bluetoothDevice.state.listen((connectState) async {
      print("监听连接设备的状态");
      if (connectState == flutter_blue.BluetoothDeviceState.connected) {
        connectDev = true;
        print("发现设备并且已经连接");
        serial_cnt = 1;
        List<flutter_blue.BluetoothService> services =
        await bluetoothDevice.discoverServices();
        services.forEach((service) async {
          print(service.uuid.toString().toLowerCase());
          print("------------------");
          // print("serviceUuid--------: ${service.uuid.toString()}");
          // 000002f0-0000-1000-8000-00805f9b34fb --- MOFEI
          // 0000ad00-0000-1000-8000-00805f9b34fb --- spp
          if (service.uuid.toString().toLowerCase() ==
              '000002f0-0000-1000-8000-00805f9b34fb') {
            print("找到02f0");
            var characteristics = service.characteristics;
            // characteristics**************************: [BluetoothCharacteristic{uuid: 000003f0-0000-1000-8000-00805f9b34fb, deviceId: D0:3D:4F:C2:DF:A3, serviceUuid: 000002f0-0000-1000-8000-00805f9b34fb, secondaryServiceUuid: null, properties: CharacteristicProperties{broadcast: false, read: true, writeWithoutResponse: false, write: true, notify: true, indicate: false, authenticatedSignedWrites: false, extendedProperties: false, notifyEncryptionRequired: false, indicateEncryptionRequired: false}, descriptors: [], value: []]

            for (flutter_blue.BluetoothCharacteristic characteristic in characteristics) {
              print("characteristicUuid--------: ${characteristic.uuid.toString()}");
              // 000003f0-0000-1000-8000-00805f9b34fb
              if (characteristic.uuid.toString() ==
                  "000003f0-0000-1000-8000-00805f9b34fb") {
                print("找到03f0");
                _blueStatesubcription.cancel();

                if (characteristicWrite == null) {
                  print("订阅");
                  characteristicWrite = characteristic;
                  print(
                      "characteristicWrite**************************: $characteristicWrite");
                  // characteristicWrite**************************: [BluetoothCharacteristic{uuid: 000003f0-0000-1000-8000-00805f9b34fb, deviceId: D0:3D:4F:C2:DF:A3, serviceUuid: 000002f0-0000-1000-8000-00805f9b34fb, secondaryServiceUuid: null, properties: CharacteristicProperties{broadcast: false, read: true, writeWithoutResponse: false, write: true, notify: true, indicate: false, authenticatedSignedWrites: false, extendedProperties: false, notifyEncryptionRequired: false, indicateEncryptionRequired: false}, descriptors: [], value: []]
                  // characteristicWrite**************************: BluetoothCharacteristic{uuid: 000003f0-0000-1000-8000-00805f9b34fb, deviceId: CD:C0:E1:5E:D7:DD, serviceUuid: 000002f0-0000-1000-8000-00805f9b34fb, secondaryServiceUuid: null, properties: CharacteristicProperties{broadcast: false, read: true, writeWithoutResponse: false, write: true, notify: true, indicate: true, authenticatedSignedWrites: false, extendedProperties: false, notifyEncryptionRequired: false, indicateEncryptionRequired: false}, descriptors: [BluetoothDescriptor{uuid: 000004f0-0000-1000-8000-00805f9b34fb, deviceId: CD:C0:E1:5E:D7:DD, serviceUuid: 000002f0-0000-1000-8000-00805f9b34fb, characteristicUuid: 000003f0-0000-1000-8000-00805f9b34fb, value: []}], value: []

                  _blueCheck = characteristicWrite.value.listen((value) {
                    print("=========================" + value.toString());
                    List result = [];
                    if (value != null && value.length > 4) {
                      result = value;
                    }
                    print("-----------------------------");
                    if (ObjectUtil.isEmptyList(result)) {
                      requestEmpty = true;
                      print("result真的为空");
                      return;
                    }

                    requestEmpty = false;
                  });

                  await characteristicWrite.setNotifyValue(true);
                } else {
                  //                  await characteristicWrite.setNotifyValue(true);
                }

                // Future.delayed(Duration(milliseconds: 1200), () {
                //   print("当前的serial_cnt： $serial_cnt");
                //   characteristicWrite
                //       .write([1, 1, (serial_cnt / 256).ceil().toInt(), serial_cnt%256], withoutResponse: false)
                //       .then((value) {})
                //       .catchError((value) {
                //     print("写入失败:$value");
                //   });
                //   serial_cnt+=1;
                // });

                _timer = Timer.periodic(Duration(milliseconds: 2000), (t) {
                  print("定时器还在准备发送");
                  if (requestEmpty == false) {
                    t.cancel();
                    return;
                  }

                  print("当前的serial_cnt： $serial_cnt");
                  characteristicWrite
                      .write([
                    1,
                    1,
                    (serial_cnt / 256).ceil().toInt(),
                    serial_cnt % 256
                  ], withoutResponse: false)
                      .then((value) {})
                      .catchError((value) {
                    print(
                        "写入失败:$value"); //写入失败:PlatformException(write_characteristic_error, writeCharacteristic failed, null, null)
                  });
                  serial_cnt += 1;
                });
              }
            }
          }
        });
      } else if (connectState == flutter_blue.BluetoothDeviceState.disconnected) {
        print('发现设备未链接');
        connectDev = false;
        //context.read<CtrApp>().setIsconnect(0);
        // if(identification == true){
        //   print('发现设备断开');
        //   context.read<CtrApp>().setIsconnect(0);
        //   disconnect();
        // }
      }
    });
    print("链接几次查看一下");
  }

  void disconnectState() {
    _blueStatesubcription = bluetoothDevice.state.listen((connectState) async {
      print("监听连接后设备的状态${connectState}");
      if (connectState == flutter_blue.BluetoothDeviceState.disconnected) {
        print('发现设备已经断开');
        if (waitDialog != null){
          print('1******');
          // waitDialog.dismiss();
          Navigator.pop(context);

        }else{
          print('2******');
        }
        //connectDev = false;
        //disconnect();
        context.read<CtrApp>().setIsconnect(0);
        // if(identification == true){
        //   print('发现设备断开');
        //   context.read<CtrApp>().setIsconnect(0);
        //   disconnect();
        // }
        _blueStatesubcription?.cancel();
        //_blueStatesubcription = null;
        _blueCheck?.cancel();
        //_blueCheck = null;
        // bluetoothDevice = null;
        // characteristicWrite = null;
        showDialog(
            useSafeArea: false,
            context: context,
            builder: (context) => CustomTwoButtonDialog(
              content: '设备已断开, 是否重新连接。',
              leftButtonTitle: '否',
              leftButtonTitleColor: Color.fromRGBO(47, 47, 47, 1),
              rightButtonTitle: '是',
              rightButtonTitleColor: Color.fromRGBO(74, 96, 136, 1),
              leftOnTap: (){
                Navigator.pop(context);
              },
              rightOnTap: (){
                Navigator.pop(context);
              },
            )
        );
      }
    });
  }


  void bleSendData(final List data) {
    int offset = 0;
    int total = data.length;
    do{
      int left = total - offset;
      int len = left > 16 ? 16 : left;

      List packet = List();
      Uint8List result = Uint8List(20);
      // result.add(0x0f);
      // result.add(left);
      // result.add((serial_cnt / 256).toInt());
      result[0] = 0x0f;
      result[1] = left;
      result[2] = (serial_cnt / 256).toInt();
      result[3] = serial_cnt % 256;
      for (int i = 0; i < len; i++) {
        packet[i + 4] = data[offset + i];
      }
      //if (serial_cnt > 0xffff) serial_cnt = 0;
      if (flutterBlue != null) {
        // app.sendmessage = byte2HexStr(packet);
        String sendMsg = packet.toString();
        // characteristic.setValue(packet);
        // characteristicWrite.writeCharacteristic(characteristic);
        serial_cnt++;
        // Log.e(TAG, "bleSendData: " + System.currentTimeMillis() + "发送的数据——" + app.sendmessage);
        // System.out.println("发送的数据=======" + app.sendmessage);

        // if (!TextUtils.isEmpty(app.sendmessage)) {
        //   if (app.istest == 1) {
        //     app.process.add(app.sendmessage);
        //   } else {
        //     app.process.add(getString(R.string.bluetooth_text_8));
        //   }
        //   //todo 查询是否写入成功
        // }
      }
      offset += len;
      if (left > 16) {
        //   try {
        //     Thread.sleep(500);
        //   } catch (InterruptedException e) {
        // e.printStackTrace();
      }
    }while(offset < total);
  }

  //蓝牙连接结果显示
  void showConnectionResult(String content) {
    ShowDialog().alertDialog(content);
  }


  void showSuccessDialogStart() {
    readDataSuccessDialog = ArsProgressDialog(context,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 200.0,
          width: 300.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.white,
          ),
          child: Column(
            children: [
              SizedBox(height: 8),
              Container(
                height: 40,
                child: Text("读取成功",
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                    textAlign: TextAlign.center),
              ),
              SizedBox(height: 8),
              Container(
                height: 40,
                child: Text("请点击确定, 继续下一步操作",
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                    textAlign: TextAlign.center),
              ),
              SizedBox(height: 16),
              Container(
                width: 100,
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: FlatButton(
                  onPressed: (){
                    if (RouterKey.navigatorKey.currentContext.read<CtrApp>().carSystem.contains('MLB')){
                      if (context.read<CtrApp>().operationType == 'readCarData'){
                        showDialog(
                            useSafeArea: false,
                            context: context,
                            builder: (context) => CustomTwoButtonDialog(
                              content: '请将设备天线靠近感应区后点击确定, 并在5秒内重复打开主驾驶车门, 直到设备发出"滴"的一声',
                              leftButtonTitle: '取消',
                              leftButtonTitleColor: Color.fromRGBO(47, 47, 47, 1),
                              rightButtonTitle: '确定',
                              rightButtonTitleColor: Color.fromRGBO(74, 96, 136, 1),
                              leftOnTap: (){
                                Navigator.pop(context);
                              },
                              rightOnTap: (){
                                Navigator.pop(context);
                                checkBluetoothTurningOn(context);
                                if(context.read<CtrApp>().isconnect>0){
                                  RouterKey.navigatorKey.currentContext.read<CtrApp>().setOperationType('audi1');
                                  // showWaitDialogStart();
                                  showWaitDialogStart2('正在采集...');
                                  RouterKey.navigatorKey.currentContext.read<CtrApp>().setHasDialogShow('0');
                                  sendMessage([0x40, 0x00, 0x01, 0x10, 0x01]);
                                }
                              },
                            ));
                      }else if (context.read<CtrApp>().operationType == 'audi1'){
                        showDialog(
                            useSafeArea: false,
                            context: context,
                            builder: (context) => CustomTwoButtonDialog(
                              content: '请将设备天线靠近感应区后点击确定, 并在5秒内重复打开关闭点火开关, 直到设备发出"滴"的一声',
                              leftButtonTitle: '取消',
                              leftButtonTitleColor: Color.fromRGBO(47, 47, 47, 1),
                              rightButtonTitle: '确定',
                              rightButtonTitleColor: Color.fromRGBO(74, 96, 136, 1),
                              leftOnTap: (){
                                Navigator.pop(context);
                              },
                              rightOnTap: (){
                                Navigator.pop(context);
                                checkBluetoothTurningOn(context);
                                if(context.read<CtrApp>().isconnect>0){
                                  RouterKey.navigatorKey.currentContext.read<CtrApp>().setOperationType('audi2');
                                  // showWaitDialogStart();
                                  showWaitDialogStart2('正在采集...');
                                  RouterKey.navigatorKey.currentContext.read<CtrApp>().setHasDialogShow('0');
                                  sendMessage([0x40, 0x00, 0x01, 0x10, 0x02]);
                                }
                              },
                            ));
                      }else if (context.read<CtrApp>().operationType == 'audi2'){
                        // showWaitDialogStart();
                        showWaitDialogStart2('数据已上传,正在获取计算结果...');
                        _requestTimer = Timer.periodic(Duration(seconds: 5), (t) {
                          requestCount++;
                          if (RouterKey.navigatorKey.currentContext.read<CtrApp>().carSystem.contains('MLB')){
                            // doGetKeyData(confirmVin, '1');
                          }else if (RouterKey.navigatorKey.currentContext.read<CtrApp>().carSystem.contains('MQB')){
                            // doGetKeyData(confirmVin, '0');
                          }
                        });
                        if (requestCount > 3){
                          _requestTimer.cancel();
                          requestCount = 0;
                          RouterKey.navigatorKey.currentState.pop();
                          ShowCustomOneButtonDialog(context: context, height: 240, title: '提示', content: '数据上传成功并已受理,点击确定退出\n请刷新APP主页的"进度查询"关注计算结果', onTap: (){
                            Navigator.pop(context);
                          }).tipsDialog();
                        }
                        // const timeout = const Duration(minutes: 5);
                        // _requestOutTimer = Timer(timeout, () { //callback function
                        //   _requestTimer.cancel();
                        // });
                      }
                    }

                    // if (readDataSuccessDialog != null){
                    //   // readDataSuccessDialog.dismiss();
                    //   RouterKey.navigatorKey.currentState.pop();
                    //   if (context.read<CtrApp>().readInfoErr == '1' && waitDialog != null){
                    //     // waitDialog.dismiss();
                    //     RouterKey.navigatorKey.currentState.pop();
                    //     context.read<CtrApp>().setReadInfoErr('0');
                    //   }
                    // }
                    // if (RouterKey.navigatorKey.currentContext.read<CtrApp>().carSystem == 'MLB系统'){
                    //   showDialog(
                    //       useSafeArea: false,
                    //       context: context,
                    //       builder: (context) => CustomTwoButtonDialog(
                    //         content: '请将设备天线靠近感应区, 并打开主驾驶室车门一次,设备发出"滴"声后点击确定。',
                    //         leftButtonTitle: '取消',
                    //         leftButtonTitleColor: Color.fromRGBO(47, 47, 47, 1),
                    //         rightButtonTitle: '确定',
                    //         rightButtonTitleColor: Color.fromRGBO(74, 96, 136, 1),
                    //         leftOnTap: (){
                    //           Navigator.pop(context);
                    //         },
                    //         rightOnTap: (){
                    //           Navigator.pop(context);
                    //           audiAction1();
                    //         },
                    //       )
                    //     );
                    //   }
                  },
                  child: Text('确定', style: TextStyle(fontSize: 14, color: Colors.black),),
                ),
              )
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    readDataSuccessDialog.show(); // show dialog
  }
  ///等待提示弹框
  Future showWaitDialogStart() {
    waitDialog = ArsProgressDialog(context,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 100.0,
          width: 100.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: CircularProgressIndicator(
            strokeWidth: 2,
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    waitDialog.show(); // show dialog
  }

  ///等待提示弹框
  Future showWaitDialogStart2(String tip) {
    waitDialog2 = ArsProgressDialog(context,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 180.0,
          width: 200.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Column(
            children: [
              SizedBox(height: 24),
              CircularProgressIndicator(
                strokeWidth: 2,
              ),
              SizedBox(height: 24),
              Text(
                  tip,
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      decoration: TextDecoration.none),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    waitDialog2.show(); // show dialog
  }

  ///带终止指令的等待提示弹框
  Future showWaitDialogStart3(String tip) {
    waitDialog3 = ArsProgressDialog(RouterKey.navigatorKey.currentContext,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 210.0,
          width: 200.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Column(
            children: [
              SizedBox(height: 24),
              CircularProgressIndicator(
                strokeWidth: 2,
              ),
              SizedBox(height: 24),
              Expanded(
                child: Text(
                    tip,
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                    textAlign: TextAlign.center),
              ),
              Container(
                height: 40,
                decoration:BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(20.0))
                ),
                child: FlatButton(
                  child: Center(
                    child: Text(
                      '终止',
                      style: TextStyle(
                          color: Colors.red
                      ),
                    ),
                  ),
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: (){
                    RouterKey.navigatorKey.currentState.pop();
                    if(RouterKey.navigatorKey.currentContext.read<CtrApp>().isconnect>0){
                      showWaitDialogStart2('终止中...');
                      sendMessage([0x40, 0x00, 0x01, 0x00, 0xF3]);
                    }else{
                      Fluttertoast.showToast(msg: '请先连接蓝牙');
                    }
                  },
                ),
              )
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    waitDialog3.show(); // show dialog
  }

  ///开始连接提示弹框
  void showConnectionStart2(BuildContext context) {
    progressDialog = ArsProgressDialog(context,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 100.0,
          width: 300.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Row(
            children: [
              CircularProgressIndicator(
                strokeWidth: 2,
              ),
              SizedBox(width: 20),
              Text("正在连接蓝牙 ...",
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      decoration: TextDecoration.none),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    progressDialog.show(); // show dialog
  }

  ///开始连接提示弹框
  void showConnectionStart() {
    progressDialog = ArsProgressDialog(context,
        blur: 2,
        dismissable: false,
        backgroundColor: Color(0x33000000),
        loadingWidget: Container(
          padding: EdgeInsets.all(20.0),
          height: 100.0,
          width: 300.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Row(
            children: [
              CircularProgressIndicator(
                strokeWidth: 2,
              ),
              SizedBox(width: 20),
              Text("正在连接蓝牙 ...",
                  style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      decoration: TextDecoration.none),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
        animationDuration: Duration(milliseconds: 500));

    progressDialog.show(); // show dialog
  }


  void disconnect() async {
    if (_blueStatesubcription != null) {
      _blueStatesubcription?.cancel();
      _blueStatesubcription = null;
    }
    if (_blueCheck != null) {
      _blueCheck?.cancel();
      _blueCheck = null;
    }
    print('蓝牙设备---${bluetoothDevice}');
    if (bluetoothDevice != null) {
      try {
        await bluetoothDevice.disconnect();
        //await characteristicWrite.setNotifyValue(false);
        characteristicWrite = null;
        //deviceName = "";
        // List<BluetoothDevice> connectedDevices = await flutterBlue.connectedDevices;
        // for (var c in connectedDevices) {
        //   print("connectedDevices: $connectedDevices, c：$c");
        //   c.disconnect();
        // }
        //bluetoothDevice = null;
      } catch (e) {
        debugPrint(e.toString());
        return;
      }

      print("disconnect断开蓝牙 $bluetoothDevice");
    }
  }

  stopScan() {
    print("停止啦");
    _scanSubscription?.cancel();
    _scanSubscription = null;
  }
}

